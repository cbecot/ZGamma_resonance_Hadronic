#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "EventLoop/OutputStream.h"

#include "CxAODMaker_VHbb/AnalysisBase_VHbb.h"

#include "CxAODMaker_VHbb/ElectronHandler_VHbb.h"
#include "CxAODMaker_VHbb/MuonHandler_VHbb.h"
#include "CxAODMaker_VHbb/FatJetHandler_VHbb.h"
#include "CxAODMaker_VHbb/JetHandler_VHbb.h"
#include "CxAODMaker/TauHandler.h"
#include "CxAODMaker_VHbb/PhotonHandler_VHbb.h"
#include "CxAODMaker/TrackJetHandler.h"
#include "CxAODMaker/TruthJetHandler.h"
#include "CxAODMaker/METHandler.h"
#include "CxAODMaker/TruthParticleHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/EventSelector.h"
#include "CxAODMaker/TruthEventHandler.h"


#include "CxAODTools_VHbb/VHbb0lepEvtSelection.h"
#include "CxAODTools_VHbb/VHbb1lepEvtSelection.h"
#include "CxAODTools_VHbb/VHbb2lepEvtSelection.h"
#include "CxAODTools_VHbb/VBFHbbInclEvtSelection.h"
#include "CxAODTools_VHbb/VBFHbb1phEvtSelection.h"

#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisBase_VHbb)


EL::StatusCode AnalysisBase_VHbb::initializeHandlers() {
  
  Info("AnalysisBase_VHbb::initializeHandlers()","Initialize handlers.");

  // initialize EventInfoHandler
  m_eventInfoHandler = new EventInfoHandler(*m_config, m_event);
  //pass information if running on derivation and MC / data to EventInfoHandler in order to have it available in the other object handlers
  m_eventInfoHandler->set_isMC(m_isMC);
  m_eventInfoHandler->set_isDerivation(m_isDerivation);
  m_eventInfoHandler->set_derivationName(m_derivationName);
  m_eventInfoHandler->set_isAFII(m_isAFII);
  EL_CHECK("AnalysisBase_VHbb::initializeHandlers()",m_eventInfoHandler->initialize());

  // note: the names of the handlers determine which collections are
  //       read, see also CxAODMaker-job.cfg
  
  // these have global pointer to be used e.g. in event selection
  if (m_isMC) {
    m_truthParticleHandler = registerHandler<TruthParticleHandler>("truthParticle");
    m_truthjetHandler = registerHandler<TruthJetHandler>("truthJet");
    m_truthfatjetHandler = registerHandler<TruthJetHandler>("truthfatJet");
    m_truthWZjetHandler = registerHandler<TruthJetHandler>("truthWZJet");
    m_truthWZfatjetHandler = registerHandler<TruthJetHandler>("truthWZfatJet");
  }
  m_muonHandler     = registerHandler<MuonHandler_VHbb>("muon");
  m_tauHandler      = registerHandler<TauHandler>("tau");
  m_electronHandler = registerHandler<ElectronHandler_VHbb>("electron");
  m_photonHandler   = registerHandler<PhotonHandler_VHbb>("photon");
  m_jetHandler      = registerHandler<JetHandler_VHbb>("jet");
  m_fatjetHandler   = registerHandler<FatJetHandler_VHbb>("fatJet");
  m_trackjetHandler = registerHandler<TrackJetHandler>("trackJet");
  m_METHandler      = registerHandler<METHandler>("MET");
  m_METHandlerMJTight= registerHandler<METHandler>("METMJTight"); 
  m_METHandlerMJMiddle= registerHandler<METHandler>("METMJMiddle"); 
  m_METHandlerMJLoose= registerHandler<METHandler>("METMJLoose"); 
  
  // these are spectators: they are calibrated and written to output,
  //                       but are not used in the event selection
   
  registerHandler<JetHandler_VHbb>("jetSpectator");
  m_METTrackHandler = registerHandler<METHandler>("METTrack");

  if (m_isMC) {
    registerHandler<METHandler>("METTruth");
    registerHandler<TruthEventHandler>("truthEvent");
  }
   
  // alternative: manual handler initialization (e.g. with different constructor)
  //  std::string containerName;
  //  std::string name = "muon";
  //  m_config->getif<std::string>(name + "Container", containerName);
  //  if ( ! containerName.empty() ) {
  //    m_muonHandler = new MuonHandler(name, *m_config, m_event, *m_eventInfoHandler);
  //    m_objectHandler.push_back( m_muonHandler );
  //  }

  // for tau truth matching
  if (m_tauHandler) {
    m_tauHandler->setTruthHandler(m_truthParticleHandler); 
  }
  

  // Set trigger stream info if data
  // need to catch embedding once the time has come
  bool m_isEmbedding=false; 
  if( !m_isMC && !m_isEmbedding ){
    TString sampleName = wk()->metaData()->castString("sample_name");
    if(sampleName.Contains("Egamma")) m_eventInfoHandler->set_TriggerStream(1);
    if(sampleName.Contains("Muons")) m_eventInfoHandler->set_TriggerStream(2);
    if(sampleName.Contains("Jet")) m_eventInfoHandler->set_TriggerStream(3);
  }
    
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_VHbb::initializeSelection() {

  Info("initializeSelection()","...");

  // determine selection name
  
  std::string selectionName = "";
  bool autoDetermineSelection;
  m_config->getif<bool>("autoDetermineSelection", autoDetermineSelection);
  if (!autoDetermineSelection) {
    m_config->getif<std::string>("selectionName", selectionName);
  } else {
    TString sampleName = wk()->metaData()->castString("sample_name");
    if      (sampleName.Contains("HIGG5D1")) selectionName = "0lep";
    else if (sampleName.Contains("HIGG5D2")) selectionName = "1lep";
    else if (sampleName.Contains("HIGG2D4")) selectionName = "2lep";
    else if (sampleName.Contains("HIGG5D3")) selectionName = "vbf"; 
    else {
      Error("initialize()", "Could not auto determine selection!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize event selection

  bool applySelection = false;
  m_config->getif<bool>("applyEventSelection", applySelection);
  if (applySelection) {
    Info("initialize()", "Applying selection: %s", selectionName.c_str());
    if      (selectionName == "0lep") m_selector->setSelection(new VHbb0lepEvtSelection());
    else if (selectionName == "1lep") m_selector->setSelection(new VHbb1lepEvtSelection());
    else if (selectionName == "2lep") m_selector->setSelection(new VHbb2lepEvtSelection());
    else if (selectionName == "vbf" ) m_selector->setSelection(new VBFHbbInclEvtSelection());
    else if (selectionName == "vbfa") m_selector->setSelection(new VBFHbb1phEvtSelection());
    else {
      Error("initialize()", "Unknown selection requested!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize overlap removal (possibly analysis specific)

  OverlapRemoval* overlapRemoval = new OverlapRemoval(*m_config);
  EL_CHECK("initializeSelection()",overlapRemoval->initialize());
  m_selector -> setOverlapRemoval(overlapRemoval);
  
  if (applySelection && (!m_muonHandler || !m_electronHandler || !m_jetHandler || !m_METHandler)) {
    Error("initialize()", "Not all collections for event selection are defined!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}
