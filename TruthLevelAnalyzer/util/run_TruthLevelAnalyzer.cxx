#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include <EventLoop/OutputStream.h>
#include "EventLoopAlgs/NTupleSvc.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>
#include <TROOT.h>
#include <TString.h>
#include <iostream>
#include <string>

#include "TruthLevelAnalyzer/TruthLevelAnalyzer.h"

using namespace std;

int main( int argc, char* argv[] )
{
    if(argc < 2)
    {
        cout << "___________________________________________________" << endl;
        
        cout << "___ WARNING : NOT ENOUGH INPUT PARAMETERS GIVEN ___" << endl;
        cout << "___________________________________________________" << endl;
        cout << "----- par 1 : output directory" << endl;
        cout << "----- par 2 : input directory" << endl;
        cout << "___________________________________________________" << endl;
        return 0;
    } else {
        cout << "______________________________________________________________________________________________________" << endl;
        cout << "_______ WILL RUN " << argv[0] << " WITH THE FOLLOWING INPUTS ________" << endl;
        for(int id = 1 ; id < argc ; id++) cout << argv[id] << endl;
        cout << "______________________________________________________________________________________________________" << endl;
    }
    
    // Take the submit directory from the input if provided:
    std::string submitDir = "submitDir";
    if( argc > 1 ) submitDir = argv[ 1 ];
    
    // Set up the job for xAOD access:
    xAOD::Init().ignore();
    
    // Construct the samples to run on:
    SH::SampleHandler sh;
    
    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
    std::string inputDir = "inputDir";
    if( argc > 2 ) inputDir = argv[ 2 ];
    const char* inputFilePath = gSystem->ExpandPathName (inputDir.data());
    cout << "Will scan the following path : " << inputFilePath << endl;
    
    SH::ScanDir().filePattern("*.root*").scan(sh, inputFilePath);
    
    // Set the name of the input TTree. It's always "CollectionTree"
    // for xAOD files.
    sh.setMetaString( "nc_tree", "CollectionTree" );
    
    // Print what we found:
    sh.print();
    
    // Load dictionnary for TTree branches creation
    gROOT->ProcessLine(".L /Users/cyril/Travail/Search_ZGamma_Resonance/TruthLevelAnalyzer/util/libloader.C+");
    
    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler( sh );
    job.options()->setDouble (EL::Job::optMaxEvents, -1);
    
    // Add an output file to the job
    string outputFileName = "analyzed_out";
    EL::OutputStream output  (outputFileName);
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc (outputFileName);
    job.algsAdd (ntuple);
    
    cout << "INPUT AND OUTPUT SET UP ! NOW CREATE THE ALGORITHM" << endl;
    // Add our analysis to the job:
    TruthLevelAnalyzer* alg = new TruthLevelAnalyzer();
    alg->m_configFile = "/Users/cyril/Travail/Search_ZGamma_Resonance/TruthLevelAnalyzer/algo_config.env";
    if( argc > 3 ) alg->m_configFile = argv[ 3 ];
    
    job.algsAdd( alg );
    alg->outputName = outputFileName;
    
    
    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.submit( job, submitDir );
    
    return 0;
}
