#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <TruthLevelAnalyzer/TruthLevelAnalyzer.h>

using namespace std;
using namespace fastjet;
using namespace fastjet::contrib;

// this is needed to distribute the algorithm to the workers
ClassImp(TruthLevelAnalyzer)



bool sort4v_byPt(TLorentzVector v1, TLorentzVector v2)
{
    return (v1.Pt() > v2.Pt());
}



TruthLevelAnalyzer :: TruthLevelAnalyzer ()
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
    configuration = new TEnv;
    if(DEBUG) Info("TruthLevelAnalyzer()"," Function TruthLevelAnalyzer");
}



void TruthLevelAnalyzer :: ReadConfig (TString config_file)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().
    if(DEBUG) Info("ReadConfig()"," Begin Function ReadConfig");
    
    cout << "WILL USE THE CONFIGURATION " << config_file << endl;
    
    configuration->ReadFile(config_file, EEnvLevel::kEnvAll);
    configuration->Print();
    
    /*** SETUP THE CONFIG ***/
    // General stuff
    DEBUG = (getConfVal<int> ("DEBUG") == 1);
    printEventsEvery = getConfVal<int> ("printEventsEvery");
    runOnNEvents = getConfVal<int> ("runOnNEvents");
    
    //Photon selection
    minPTgamma = getConfVal<double> ("minPTgamma");
    
    //Jets selection
    nbRequiredPreselJets = getConfVal<int> ("nbRequiredPreselJets");
    minPTjet = getConfVal<double> ("minPTjet");
    PDG_MZ = getConfVal<double> ("PDG_MZ");
    
    //Jet algorithm
    TString tmpJetAlgType = getConfVal<TString> ("used_jet_algorithm");
    if (tmpJetAlgType == "antikt") used_jet_algorithm = antikt_algorithm;
    else if (tmpJetAlgType == "kt") used_jet_algorithm = kt_algorithm;
    else {
        cout << "Missing jet algorithm ! EXITING !" << endl;
        exit(0);
    }
    
    if(DEBUG) Info("ReadConfig()"," End Function ReadConfig");
}



EL::StatusCode TruthLevelAnalyzer :: setupJob (EL::Job& job)
{
    // Here you put code that sets up the job on the submission object
    // so that it is ready to work with your algorithm, e.g. you can
    // request the D3PDReader service or add output files.  Any code you
    // put here could instead also go into the submission script.  The
    // sole advantage of putting it here is that it gets automatically
    // activated/deactivated when you add/remove the algorithm from your
    // job, which may or may not be of value to you.
    if(DEBUG) Info("setupJob()"," Begin function setupJob");
    //---------------------------------------------
    
    job.useXAOD ();
    EL_CHECK( "setupJob()", xAOD::Init() );
    
    //---------------------------------------------
    if(DEBUG) Info("setupJob()"," End function setupJob");
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: histInitialize ()
{
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: fileExecute ()
{
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: changeInput (bool firstFile)
{
    // Here you do everything you need to do when we change input files,
    // e.g. resetting branch addresses on trees.  If you are using
    // D3PDReader or a similar service this method is not needed.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: initialize ()
{
    // Here you do everything that you need to do after the first input
    // file has been connected and before the first event is processed,
    // e.g. create additional histograms based on which variables are
    // available in the input files.  You can also create all of your
    // histograms and trees in here, but be aware that this method
    // doesn't get called if no events are processed.  So any objects
    // you create here won't be available in the output if you have no
    // input events.
    
    if(DEBUG) Info("initialize()"," Begin function initialize");
    ANA_CHECK_SET_TYPE (EL::StatusCode);
    //---------------------------------------------
    
    xAOD::TEvent* event = wk()->xaodEvent();
    
    ReadConfig(m_configFile);
    
    const xAOD::EventInfo* eventInfo = 0;
    ANA_CHECK( event->retrieve(eventInfo, "EventInfo") );
    m_isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
    if(m_isMC) cout << "***** FILE TYPE INFO : THIS IS MC *****" << endl;
    else cout << "***** FILE TYPE INFO : THIS IS DATA *****" << endl;
    
    m_cutflowNamesReady = false;
    m_nevents = 0;
    
    out_file = wk()->getOutputFile (outputName);
    if(out_file == NULL) return EL::StatusCode::FAILURE;
    
    out_cutflow = defineOutputHist ("cutflow", 100, -0.5, 99.5);
    out_cutflow_preselectedPhotons = defineOutputHist ("cutflowPreselPhotons", 100, -0.5, 99.5);
    out_cutflow_preselectedJets = defineOutputHist ("cutflowPreselJets", 100, -0.5, 99.5);
    out_tree = defineOutputTree ("ZGamma_Tree");
    
    AddVarToTree< vector< vector<TLorentzVector> > > (m_jet4v_antikt, "jet4v_antikt");
    AddVarToTree< vector< vector<double> > > (m_jet_tau21, "jet_tau21");
    AddVarToTree< vector< vector<double> > > (m_jet_tau32, "jet_tau32");
    AddVarToTree< vector< vector<double> > > (m_jet_d2, "jet_d2");
    AddVarToTree< double > (m_jet_radius, "jet_radius");
    AddVarToTree< vector<TLorentzVector> > (m_hadron4v, "hadron4v");
    AddVarToTree< vector<TLorentzVector> > (m_parton4v, "parton4v");
    AddVarToTree< vector<TLorentzVector> > (m_photon4v, "photon4v");
    AddVarToTree< vector<TLorentzVector> > (m_z4v, "z4v");
    AddVarToTree< vector<TLorentzVector> > (m_jjg4v, "jjg4v");
    AddVarToTree< vector<TLorentzVector> > (m_Zdaughters, "Zdaughters");
    AddVarToTree< vector<double> > (m_DeltaR_jets, "DeltaR_jets");
    AddVarToTree< bool > (m_foundZboson, "foundZboson");
    AddVarToTree< vector< map<int, pair<int,int> > > > (m_treeJet, "treeJet");
    AddVarToTree< vector< map<int, vector<double> > > > (m_treeJet_content, "treeJet_content");
    AddVarToTree< vector<int> > (m_treeJet_startingNode, "treeJet_startingNode");
    AddVarToTree< vector< map<TString,double> > > (m_jets_initParticule_PDGIDs, "jets_initParticule_PDGIDs");
    AddVarToTree< vector< map<TString,double> > > (m_jets_initParticule_mother_PDGIDs, "jets_initParticule_mother_PDGIDs");
    
    //For the jets (re)building
    m_jet_radius = 1.0; //, 0.4};
    
    //---------------------------------------------
    if(DEBUG) Info("initialize()"," End function initialize");
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: execute ()
{
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.
    
    if( (runOnNEvents > 0)&&(m_nevents > runOnNEvents) ) return EL::StatusCode::SUCCESS;
    if(DEBUG) Info("execute()"," Begin function execute");
    ANA_CHECK_SET_TYPE (EL::StatusCode);
    //---------------------------------------------
    
    /**************************************************************/
    
    //-----------------------------
    //----- Recover the event -----
    if( (m_nevents+1)%printEventsEvery == 0 ) Info("execute()", "--- Passing entry nb %lli ---",  m_nevents+1);
    ANA_CHECK( ClearPreviousEvent() );
    do_cutflow( "Initial" );
    xAOD::TEvent* event = wk()->xaodEvent();
    
    const xAOD::TruthEventContainer* TruthLevelEvents = NULL;
    ANA_CHECK( event->retrieve(TruthLevelEvents,"TruthEvents") );
    if(DEBUG) cout << "Nb events : " << TruthLevelEvents->size() << endl;
    const xAOD::TruthEvent* truthEvent = TruthLevelEvents->at(0);
    
    
    
    //--------------------------------------------
    //----- Retrieve hard interaction vertex -----
    const xAOD::TruthVertex* hardprocess_vertex = truthEvent->signalProcessVertex();
    
    if(DEBUG)
    {
        const xAOD::TruthVertexContainer* TruthVertices = NULL;
        ANA_CHECK( event->retrieve(TruthVertices,"TruthVertices") );
        if(DEBUG) cout << "_____*** Nb vertices : " << TruthVertices->size() << endl;
        
        
        if(DEBUG) cout <<"OUTGOING PARTICLES "<< hardprocess_vertex->nIncomingParticles() << endl;
        for(unsigned int idpart = 0 ; idpart < hardprocess_vertex->nIncomingParticles() ; ++idpart) {
            if(DEBUG) cout << "BEGIN" << endl;
            const xAOD::TruthParticle* currentpart = hardprocess_vertex->incomingParticle(idpart);
            if(DEBUG) {
                cout << __LINE__ << " " << currentpart->pdgId() << " " << currentpart->isZ() << " " << currentpart->isPhoton() << endl;
            }
            if(DEBUG) cout << "END" << endl;
        }
    }
    
    
    
    //----------------------------------------------------------------
    //-----  Retrieve all particles from hard interaction vertex -----
    const xAOD::TruthParticleContainer* all_particles = NULL;
    ANA_CHECK( event->retrieve(all_particles, "TruthParticles") );
    if(DEBUG) cout << __LINE__ << " " << all_particles->size() << endl;
    
    
    //------------------------------
    //-----  Retrieve photons  -----
    xAOD::TruthParticleContainer* init_photons = createContainer<xAOD::TruthParticleContainer> ();
    
    for(const xAOD::TruthParticle* current_particle : *all_particles) {
        if( (current_particle->status() < 1)||(current_particle->status() == 4) ) continue; //Select final-state particles
        if( !current_particle->isPhoton() ) continue;
        if( current_particle->pt() < minPTgamma*1000. ) continue;
        if( current_particle->eta() > 2.4 ) continue;
        
        copyConstObjectToCollection<xAOD::TruthParticleContainer, xAOD::TruthParticle> (init_photons, current_particle);
    }
    
    if(init_photons->size() < 1) return EL::StatusCode::SUCCESS;
    do_cutflow("Nb. photons");
    do_cutflow_obj("Initial", out_cutflow_preselectedPhotons, m_step_photons, init_photons->size());
    
    
    //-----------------------------
    //-----  Retrieve bosons  -----
    xAOD::TruthParticleContainer* init_Zbosons = createContainer<xAOD::TruthParticleContainer> ();
    
    for(const xAOD::TruthParticle* current_particle : *all_particles) {
        if( DEBUG && current_particle->isHiggs() )
            cout << "Higgs boson mother # : " << current_particle->nParents() << endl;
        
        if( current_particle->isZ()) {
            if(DEBUG) cout << "Z boson mother # : " << current_particle->nParents() << endl;
            copyConstObjectToCollection<xAOD::TruthParticleContainer, xAOD::TruthParticle> (init_Zbosons, current_particle);
            m_foundZboson = true;
        }
    }
    
    if(init_photons->size() < 1) return EL::StatusCode::SUCCESS;
    do_cutflow("Nb. Z bosons");
    
    
    //------------------------------
    //-----  Retrieve hadrons  -----
    xAOD::TruthParticleContainer* init_hadrons = createContainer<xAOD::TruthParticleContainer> ();
    
    for(const xAOD::TruthParticle* current_particle : *all_particles) {
        if( !(current_particle->status() == 1) ) continue; //Select final-state particles
        if( !current_particle->isHadron() ) continue;
        if(current_particle->pt() < 0.*1000.) continue;
        if( fabs(current_particle->eta()) > 5 ) continue;
        
        copyConstObjectToCollection<xAOD::TruthParticleContainer, xAOD::TruthParticle> (init_hadrons, current_particle);
        
    }
    
    vector<PseudoJet> pseudojets_clusteringInput;
    for(auto current_hadron : *init_hadrons) {
        pseudojets_clusteringInput.push_back( PseudoJet(current_hadron->px(), current_hadron->py(), current_hadron->pz(), current_hadron->e()) );
        if(DEBUG) cout << "Current pseudo-jet phi : " << pseudojets_clusteringInput.back().phi_std() << endl;
    }
    
    if(init_photons->size() < 1) return EL::StatusCode::SUCCESS;
    do_cutflow("Nb. Photons");
    
    
    //-----------------------------
    //-----  Retrieve partons  -----
    xAOD::TruthParticleContainer* init_partons = createContainer<xAOD::TruthParticleContainer> ();
    
    for(const xAOD::TruthParticle* current_particle : *all_particles) {
        if( !current_particle->isParton() ) continue;
        if(current_particle->pt() < 10.*1000.) continue;
        if( fabs(current_particle->eta()) > 5 ) continue;
        
        copyConstObjectToCollection<xAOD::TruthParticleContainer, xAOD::TruthParticle> (init_partons, current_particle);
        m_parton4v.push_back( current_particle->p4() );
    }
    
    sort(m_parton4v.begin(), m_parton4v.end(), sort4v_byPt);
    
    
    
    //------------------------------------------------
    //----- Fill some information for the output -----
    
    for(auto current_ph : *init_photons) m_photon4v.push_back( current_ph->p4() );
    sort(m_photon4v.begin(), m_photon4v.end(), sort4v_byPt);
    
    for(auto current_z : *init_Zbosons) {
        if(!m_foundZboson) continue;
        m_z4v.push_back( current_z->p4() );
        
        for(size_t id_child = 0 ; id_child < current_z->nChildren() ; ++id_child)
            m_Zdaughters.push_back( current_z->child(id_child)->p4() );
    }
    
    
    for(auto current_jet : *init_hadrons) m_hadron4v.push_back( current_jet->p4() );
    sort(m_hadron4v.begin(), m_hadron4v.end(), sort4v_byPt);
    
    if(false) { //m_foundZboson) {
        if(m_Zdaughters.size() > 1 && m_photon4v.size() > 0)
            m_jjg4v.push_back( m_Zdaughters[0]+m_Zdaughters[1]+m_photon4v[0] );
        
        m_DeltaR_jets.push_back( m_Zdaughters[0].DeltaR( m_Zdaughters[1] ) );
    } else {
        if(m_parton4v.size() > 1 && m_photon4v.size() > 0)
            m_jjg4v.push_back( m_parton4v[0]+m_parton4v[1]+m_photon4v[0] );
        
        m_DeltaR_jets.push_back( m_parton4v[0].DeltaR( m_parton4v[1] ) );
    }
    
    
    
    
    //---------------------------------------------------
    //----- Build the jets and fill the information -----
    
    //1st pass : retrieve all the jets inside the event//
    /***************************************************/
    JetDefinition jet_def_antikt(used_jet_algorithm, m_jet_radius);
    ClusterSequence cs(pseudojets_clusteringInput, jet_def_antikt);
    
    
    vector<PseudoJet> jets = sorted_by_pt(cs.inclusive_jets(minPTjet*1000)); //min PT jet of 20 GeV
    
    m_jet4v_antikt.push_back( vector<TLorentzVector> () );
    m_jet_tau21.push_back( vector<double> () );
    m_jet_tau32.push_back( vector<double> () );
    m_jet_d2.push_back( vector<double> () );
    
    for(auto current_jet : jets) {
      m_jet4v_antikt.back().push_back( TLorentzVector(-1, -1, -1, -1) );
      m_jet4v_antikt.back().back().SetPxPyPzE(current_jet.px(), current_jet.py(), current_jet.pz(), current_jet.e());
        
        
        NsubjettinessRatio   nSub21_beta1(2,1, OnePass_WTA_KT_Axes(), UnnormalizedMeasure(1.0));
        NsubjettinessRatio   nSub32_beta1(3,2, OnePass_WTA_KT_Axes(), UnnormalizedMeasure(1.0));
        m_jet_tau21.back().push_back( nSub21_beta1(current_jet) );
        m_jet_tau32.back().push_back( nSub32_beta1(current_jet) );
        
        auto ecf1calc = EnergyCorrelator(1, 1, EnergyCorrelator::pt_R);
        auto ecf2calc = EnergyCorrelator(2, 1, EnergyCorrelator::pt_R);
        auto ecf3calc = EnergyCorrelator(3, 1, EnergyCorrelator::pt_R);
        
        double ecf1 = ecf1calc(current_jet);
        double ecf2 = ecf2calc(current_jet);
        double ecf3 = ecf3calc(current_jet);
        
        double currentd2;
        if(fabs(ecf2) > 1e-8)  currentd2 = ecf3 * pow(ecf1, 3.0) / pow(ecf2, 3.0);
        else currentd2 = -999.0;
        
        m_jet_d2.back().push_back( currentd2 );
    }
    
    
    
    //2nd pass : separately get each jets constituents //
    //           and rebuild the jets (to get history) //
    /***************************************************/
    for(auto current_jet : jets)
    {
        JetDefinition jet_def_antikt_recluster(used_jet_algorithm, m_jet_radius);
        ClusterSequence cs_recluster(current_jet.constituents(), jet_def_antikt_recluster);
        
        if(DEBUG) {
            cout << endl << endl << "________________________________________" << endl;
            cout << "Current jet 4v : " << current_jet.px() << " " << current_jet.py() << " " <<  current_jet.pz() << " " <<  current_jet.e() << endl;
            for(auto current_hist_elem : cs_recluster.history()) {
                cout << "Jet index = " << current_hist_elem.jetp_index << " ; Parent1 = " <<    current_hist_elem.parent1 << " ; Parent2 = " << current_hist_elem.parent2 << " ; Child = " << current_hist_elem.child << " ; dij = " << current_hist_elem.dij << " " << cs_recluster.jets()[current_hist_elem.jetp_index].px() << " " << cs_recluster.jets()[current_hist_elem.jetp_index].py() << " " << cs_recluster.jets()[current_hist_elem.jetp_index].pz() << " " << cs_recluster.jets()[current_hist_elem.jetp_index].e() << endl;
            }
            cout << "________________________________________" << endl << endl << endl;
        }
        
        //3rd step : Now save the history//
        /*********************************/
        stack<int> starting_point;
        
        if(DEBUG) cout << __FILE__ << __LINE__ << endl;
        for(size_t idnode = 0 ; idnode < cs_recluster.history().size() ; ++idnode) {
            //auto current_hist_elem : cs_recluster.history()){
            auto current_hist_elem = cs_recluster.history()[idnode];
            if(current_hist_elem.jetp_index == -3) {
                if(current_hist_elem.parent1 > -1) starting_point.push(current_hist_elem.parent1);
                if(current_hist_elem.parent2 > -1) starting_point.push(current_hist_elem.parent2);
                break;
            }
        }
        if(DEBUG) cout << __FILE__ << __LINE__ << endl;
        m_treeJet_startingNode.push_back( starting_point.top() );
        m_treeJet.push_back( map<int, pair<int, int> > () );
        m_treeJet_content.push_back( map<int, vector<double> > () );
        if(DEBUG) cout << __FILE__ << __LINE__ << endl;
        
        while(!starting_point.empty()) {
            if(DEBUG) cout << __FILE__ << __LINE__ << endl;
            int topNode = starting_point.top();
            if(DEBUG) cout << __FILE__ << __LINE__ << " " << topNode << endl;
            int p1 = cs_recluster.history()[topNode].parent1;
            int p2 = cs_recluster.history()[topNode].parent2;
            m_treeJet.back()[topNode] = pair<int,int> (p1,p2);
            if(DEBUG) cout << __FILE__ << __LINE__ << endl;
            
            
            starting_point.pop();
            
            if(p1 > -1) starting_point.push(p1);
            if(p2 > -1) starting_point.push(p2);
            
            if(DEBUG) cout << __FILE__ << __LINE__ << " " << topNode << endl;
            m_treeJet_content.back()[topNode] = vector<double> (7, 0);
            m_treeJet_content.back()[topNode][0] = cs_recluster.history()[topNode].dij;
            
            if(DEBUG) cout << __FILE__ << __LINE__ << endl;
            int jetpind = cs_recluster.history()[topNode].jetp_index;
            m_treeJet_content.back()[topNode][1] = cs_recluster.jets()[jetpind].px();
            m_treeJet_content.back()[topNode][2] = cs_recluster.jets()[jetpind].py();
            m_treeJet_content.back()[topNode][3] = cs_recluster.jets()[jetpind].pz();
            m_treeJet_content.back()[topNode][4] = cs_recluster.jets()[jetpind].e();
            if(DEBUG) cout << __FILE__ << __LINE__ << endl;

        }
        
        
        if(DEBUG) cout << __FILE__ << __LINE__ << endl;
    }
    
    
    if(DEBUG) cout << __FILE__ << __LINE__ << endl;
    //4th step : find the closest parton to each jet and associate it//
    /*****************************************************************/
    for(auto current_jet4 : m_jet4v_antikt[0]) {
        double currentHighestE = -1;
        map<TString,double> currentAssociation;
        map<TString,double> currentAssociation_mother;
        
        for(const xAOD::TruthParticle* current_particle : *all_particles)
        {
            if( (current_particle->status() < 1)||(current_particle->status() == 4) )  continue;
            if(!current_particle->isParton()) continue;
            if(current_particle->p4().DeltaR(current_jet4) > m_jet_radius) continue;
            
            if(current_particle->p4().E() > currentHighestE) {
                currentHighestE = current_particle->p4().E();
                
                currentAssociation["PDGID"] = current_particle->pdgId();
                currentAssociation["MASS"] = current_particle->m();
                currentAssociation["CHARGE"] = current_particle->charge();
                currentAssociation["CHARM"] = current_particle->hasCharm();
                currentAssociation["BOTTOM"] = current_particle->hasBottom();
                currentAssociation["PARTON"] = current_particle->isParton();
                currentAssociation["TOP"] = current_particle->isTop();
                currentAssociation["W"] = current_particle->isW();
                currentAssociation["Z"] = current_particle->isZ();
                currentAssociation["H"] = current_particle->isHiggs();
                currentAssociation["STATUS"] = current_particle->status();
                
                currentAssociation_mother["PDGID"] = current_particle->parent()->pdgId();
                currentAssociation_mother["MASS"] = current_particle->parent()->m();
                currentAssociation_mother["CHARGE"] = current_particle->parent()->charge();
                currentAssociation_mother["CHARM"] = current_particle->parent()->hasCharm();
                currentAssociation_mother["BOTTOM"] = current_particle->parent()->hasBottom();
                currentAssociation_mother["PARTON"] = current_particle->parent()->isParton();
                currentAssociation_mother["TOP"] = current_particle->parent()->isTop();
                currentAssociation_mother["W"] = current_particle->parent()->isW();
                currentAssociation_mother["Z"] = current_particle->parent()->isZ();
                currentAssociation_mother["H"] = current_particle->parent()->isHiggs();
                currentAssociation_mother["STATUS"] = current_particle->parent()->status();
            }
        }
        
        m_jets_initParticule_PDGIDs.push_back(currentAssociation);
        m_jets_initParticule_mother_PDGIDs.push_back(currentAssociation_mother);
    }
    
    
    
    if(DEBUG) cout << "TREE SIZE : " << m_treeJet_content.size() /*.size()*/ << endl;
    do_cutflow("Final");
    out_tree->Fill();
    //---------------------------------------------
    if(DEBUG) Info("execute()"," End function execute");
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: postExecute ()
{
    // Here you do everything that needs to be done after the main event
    // processing.  This is typically very rare, particularly in user
    // code.  It is mainly used in implementing the NTupleSvc.
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: finalize ()
{
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.  This is different from histFinalize() in that it only
    // gets called on worker nodes that processed input events.
    
    if(DEBUG) Info("finalize()"," Begin function finalize");
    ANA_CHECK_SET_TYPE (EL::StatusCode);
    //---------------------------------------------
    
    cout << "--- " << m_nevents << " have been read ---" << endl;
    cout << "--- End algorithm TruthLevelAnalyzer ---" << endl;
    
    //---------------------------------------------
    if(DEBUG) Info("finalize()"," End function finalize");
    
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: ClearPreviousEvent()
{
    if(DEBUG) Info("ClearPreviousEvent()"," Begin function ClearPreviousEvent");
    //---------------------------------------------
    
    m_cutflow = 0;
    m_step_photons = 0;
    m_step_jets = 0;
    m_nevents++;
    m_foundZboson = false;
    
    m_jet4v_antikt.clear();
    m_jet_tau21.clear();
    m_jet_tau32.clear();
    m_jet_d2.clear();
    m_hadron4v.clear();
    m_parton4v.clear();
    m_photon4v.clear();
    m_z4v.clear();
    m_jjg4v.clear();
    m_Zdaughters.clear();
    m_DeltaR_jets.clear();
    m_treeJet.clear();
    m_treeJet_content.clear();
    m_treeJet_startingNode.clear();
    m_jets_initParticule_PDGIDs.clear();
    m_jets_initParticule_mother_PDGIDs.clear();
    
    //---------------------------------------------
    if(DEBUG) Info("ClearPreviousEvent()"," End function ClearPreviousEvent");
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode TruthLevelAnalyzer :: histFinalize ()
{
    // This method is the mirror image of histInitialize(), meaning it
    // gets called after the last event has been processed on the worker
    // node and allows you to finish up any objects you created in
    // histInitialize() before they are written to disk.  This is
    // actually fairly rare, since this happens separately for each
    // worker node.  Most of the time you want to do your
    // post-processing on the submission node after all your histogram
    // outputs have been merged.  This is different from finalize() in
    // that it gets called on all worker nodes regardless of whether
    // they processed input events.
    return EL::StatusCode::SUCCESS;
}
