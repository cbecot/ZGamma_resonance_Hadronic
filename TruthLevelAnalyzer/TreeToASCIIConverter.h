//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun 15 20:13:08 2016 by ROOT version 6.04/14
// from TTree ZGamma_Tree/ZGamma_Tree
// found on file: testTruthDxAOD0.root
//////////////////////////////////////////////////////////

#ifndef TreeToASCIIConverter_h
#define TreeToASCIIConverter_h

#include <TROOT.h>
#include <TChain.h>
#include <TString.h>
#include <TFile.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

#include <TLorentzVector.h>
#include <map>
#include <utility>
#include <fstream>
#include <iostream>

using namespace std;

class TreeToASCIIConverter {
    public :
    TTree          *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t           fCurrent; //!current Tree number in a TChain
    
    // Fixed size dimensions of array or collections stored in the TTree if any.
    
    // Declaration of leaf types
    TString outputASCIIFile;
    TString outputROOTFile;
    bool issignalfile;
    
    vector<vector<TLorentzVector> > *jet4v_antikt;
    vector<vector<double> > *jet_tau21;
    vector<vector<double> > *jet_tau32;
    vector<vector<double> > *jet_d2;
    Double_t        jet_radius;
    vector<TLorentzVector> *hadron4v;
    vector<TLorentzVector> *parton4v;
    vector<TLorentzVector> *photon4v;
    vector<TLorentzVector> *z4v;
    vector<TLorentzVector> *jjg4v;
    vector<TLorentzVector> *Zdaughters;
    vector<double>  *DeltaR_jets;
    Bool_t          foundZboson;
    vector<map<int,pair<int,int> > > *treeJet;
    vector<map<int,vector<double> > > *treeJet_content;
    vector<int>     *treeJet_startingNode;
    vector< map<TString,double> >     *jets_initParticule_PDGIDs;
    vector< map<TString,double> >     *jets_initParticule_mother_PDGIDs;
    
    // List of branches
    TBranch        *b_jet4v_antikt;   //!
    TBranch        *b_jet_tau21;   //!
    TBranch        *b_jet_tau32;   //!
    TBranch        *b_jet_d2;   //!
    TBranch        *b_jet_radius;   //!
    TBranch        *b_hadron4v;   //!
    TBranch        *b_parton4v;   //!
    TBranch        *b_photon4v;   //!
    TBranch        *b_z4v;   //!
    TBranch        *b_jjg4v;   //!
    TBranch        *b_Zdaughters;   //!
    TBranch        *b_DeltaR_jets;   //!
    TBranch        *b_foundZboson;   //!
    TBranch        *b_treeJet;   //!
    TBranch        *b_treeJet_content;   //!
    TBranch        *b_treeJet_startingNode;   //!
    TBranch        *b_jets_initParticule_PDGIDs;   //!
    TBranch        *b_jets_initParticule_mother_PDGIDs;   //!
   
    TreeToASCIIConverter(TString filename="testTruthDxAOD0.root", bool typefile=true, TTree *tree=0);
    virtual ~TreeToASCIIConverter();
    virtual Int_t    Cut(Long64_t entry);
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop();
    virtual Bool_t   Notify();
    virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef TreeToASCIIConverter_cxx
TreeToASCIIConverter::TreeToASCIIConverter(TString filename, bool typefile, TTree *tree) : fChain(0)
{
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(filename);
        if (!f || !f->IsOpen()) {
            f = new TFile(filename);
        }
        f->GetObject("ZGamma_Tree",tree);
        
    }
    
    issignalfile = typefile;    
    outputASCIIFile = filename;
    gSystem->ExpandPathName(outputASCIIFile);
    outputASCIIFile.ReplaceAll(".root",".dat");    
    cout << "Will read " << tree->GetEntries() << " in file " << filename << endl;
    cout << "Will write into the file : " << outputASCIIFile << endl;

    outputROOTFile = filename;
    outputASCIIFile.ReplaceAll(".root",".dat");
    outputROOTFile.ReplaceAll(".root","_output.root");
    
    Init(tree);
}

TreeToASCIIConverter::~TreeToASCIIConverter()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t TreeToASCIIConverter::GetEntry(Long64_t entry)
{
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t TreeToASCIIConverter::LoadTree(Long64_t entry)
{
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void TreeToASCIIConverter::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).
    
    // Set object pointer
    jet4v_antikt = 0;
    jet_tau21 = 0;
    jet_tau32 = 0;
    jet_d2 = 0;
    hadron4v = 0;
    parton4v = 0;
    photon4v = 0;
    z4v = 0;
    jjg4v = 0;
    Zdaughters = 0;
    DeltaR_jets = 0;
    treeJet = 0;
    treeJet_content = 0;
    treeJet_startingNode = 0;
    jets_initParticule_PDGIDs = 0;
    jets_initParticule_mother_PDGIDs = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
    
    fChain->SetBranchAddress("jet4v_antikt", &jet4v_antikt, &b_jet4v_antikt);
    fChain->SetBranchAddress("jet_tau21", &jet_tau21, &b_jet_tau21);
    fChain->SetBranchAddress("jet_tau32", &jet_tau32, &b_jet_tau32);
    fChain->SetBranchAddress("jet_d2", &jet_d2, &b_jet_d2);
    fChain->SetBranchAddress("jet_radius", &jet_radius, &b_jet_radius);
    fChain->SetBranchAddress("hadron4v", &hadron4v, &b_hadron4v);
    fChain->SetBranchAddress("parton4v", &parton4v, &b_parton4v);
    fChain->SetBranchAddress("photon4v", &photon4v, &b_photon4v);
    fChain->SetBranchAddress("z4v", &z4v, &b_z4v);
    fChain->SetBranchAddress("jjg4v", &jjg4v, &b_jjg4v);
    fChain->SetBranchAddress("Zdaughters", &Zdaughters, &b_Zdaughters);
    fChain->SetBranchAddress("DeltaR_jets", &DeltaR_jets, &b_DeltaR_jets);
    fChain->SetBranchAddress("foundZboson", &foundZboson, &b_foundZboson);
    fChain->SetBranchAddress("treeJet", &treeJet, &b_treeJet);
    fChain->SetBranchAddress("treeJet_content", &treeJet_content, &b_treeJet_content);
    fChain->SetBranchAddress("treeJet_startingNode", &treeJet_startingNode, &b_treeJet_startingNode);
    fChain->SetBranchAddress("jets_initParticule_PDGIDs", &jets_initParticule_PDGIDs, &b_jets_initParticule_PDGIDs);
    fChain->SetBranchAddress("jets_initParticule_mother_PDGIDs", &jets_initParticule_mother_PDGIDs, &b_jets_initParticule_mother_PDGIDs);
    Notify();
}

Bool_t TreeToASCIIConverter::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.
    
    return kTRUE;
}

void TreeToASCIIConverter::Show(Long64_t entry)
{
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t TreeToASCIIConverter::Cut(Long64_t entry)
{
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}
#endif // #ifdef TreeToASCIIConverter_cxx
