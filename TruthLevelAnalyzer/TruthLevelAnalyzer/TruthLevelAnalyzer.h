#ifndef TruthLevelAnalyzer_TruthLevelAnalyzer_H
#define TruthLevelAnalyzer_TruthLevelAnalyzer_H

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>
#include <EventLoop/OutputStream.h>
#include <EventLoopAlgs/NTupleSvc.h>

#include <AsgTools/MessageCheck.h>

#include <xAODCore/AuxContainerBase.h>
#include <xAODRootAccess/Init.h>
#include <xAODRootAccess/TEvent.h>

#include "TruthLevelAnalyzer/helpers_header.h"

#include <vector>
#include <stack>
#include <string>
#include <utility>
#include <iostream>

#include <TClonesArray.h>
#include <TObject.h>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TH2.h>
#include <TStyle.h>
#include <TTree.h>
#include <TH1F.h>
#include <TLorentzVector.h>
#include <TEnv.h>
#include <TString.h>

#include <PathResolver/PathResolver.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/Jet.h>
#include <xAODJet/JetContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODTruth/TruthEvent.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODCore/AuxContainerBase.h>

#include <fastjet/PseudoJet.hh>
#include <fastjet/ClusterSequence.hh>
#include <fastjet/JetDefinition.hh>
#include <fastjet/contrib/Nsubjettiness.hh>
#include <fastjet/contrib/EnergyCorrelator.hh>


#define SAFE_DELETE(x) { if (x) { delete x; x=0; } }





class TruthLevelAnalyzer : public EL::Algorithm
{
    // put your configuration variables here as public variables.
    // that way they can be set directly from CINT and python.
public:
    // float cutValue;
    
    
    
    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
public:
    TEnv* configuration; //!
    TString m_configFile;
    
    // General stuff
    bool DEBUG;
    int printEventsEvery;
    int runOnNEvents;
    
    //Photon selection
    double minPTgamma;
    
    //Jets selection
    unsigned int nbRequiredPreselJets;
    double minPTjet;
    double PDG_MZ;
    
    //Jet algorithm settings
    fastjet::JetAlgorithm used_jet_algorithm;
    
    
    /*****************************************************/
    /*** Things to be saved or needed to do the saving ***/
    std::string outputName;
    TFile* out_file; //!
    TTree* out_tree; //!
    TH1F* out_cutflow; //!
    TH1F* out_cutflow_preselectedPhotons; //!
    TH1F* out_cutflow_preselectedJets; //!
    
    std::vector<TLorentzVector> m_Zdaughters; //!
    std::vector< std::vector<TLorentzVector> > m_jet4v_antikt; //!
    std::vector< std::vector<double> > m_jet_tau21; //!
    std::vector< std::vector<double> > m_jet_tau32; //!
    std::vector< std::vector<double> > m_jet_d2; //!
    double m_jet_radius; //!
    std::vector<TLorentzVector> m_hadron4v; //!
    std::vector<TLorentzVector> m_parton4v; //!
    std::vector<TLorentzVector> m_z4v; //!
    std::vector<TLorentzVector> m_photon4v; //!
    std::vector<TLorentzVector> m_jjg4v; //!
    std::vector<double> m_DeltaR_jets; //!
    std::vector< std::map<int, std::pair<int,int> > > m_treeJet; //!
    std::vector< std::map<int, std::vector<double> > > m_treeJet_content; //!
    std::vector<int> m_treeJet_startingNode; //!
    std::vector< std::map<TString,double> > m_jets_initParticule_PDGIDs; //!
    std::vector< std::map<TString,double> > m_jets_initParticule_mother_PDGIDs; //!
    bool m_foundZboson; //!
    
    /****************************************/
    /***        Internal variables        ***/
    int m_cutflow; //!
    int m_step_photons; //!
    int m_step_jets; //!
    Long64_t m_nevents; //!
    bool m_cutflowNamesReady; //!
    bool m_isMC; //!

    
    
    /****************************************/
    // this is a standard constructor
    TruthLevelAnalyzer ();
    TruthLevelAnalyzer (TString config_file);
    
    //Will read the file summarizing the TEnv
    void ReadConfig (TString config_file);
    
    //Event-level functions
    EL::StatusCode ClearPreviousEvent();
    
    //Photon-level functions
    EL::StatusCode photon_preselection(xAOD::TruthParticleContainer* presel_ph, const xAOD::TruthParticleContainer* init_ph);
    //Jet-level functions
    EL::StatusCode jet_preselection(xAOD::JetContainer* jets);
    
    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();
    
    
    
    /****************************************/
    // inside useful functions
    inline void do_cutflow(const char* step, double weight=1)
    {
        if(DEBUG) Info("do_cutflow()",">>>>> STEP %s <<<<<", step );
        if(DEBUG) std::cout << out_cutflow << std::endl;
        out_cutflow->Fill(m_cutflow, weight); m_cutflow++;
        if(!m_cutflowNamesReady) {
            out_cutflow->GetXaxis()->SetBinLabel(m_cutflow, step);
        }
    }
    
    inline void do_cutflow_obj(const char* step, TH1F* h_cut, int& countstep, double weight)
    {
        if(DEBUG) Info("do_cutflow_obj()",">>>>> STEP %s for %s <<<<<", step, h_cut->GetName() );
        h_cut->Fill(countstep, weight); countstep++;
        if(!m_cutflowNamesReady) {
            h_cut->GetXaxis()->SetBinLabel(countstep, step);
        }
    }
    
    template<typename typecontainer, typename typeobject> inline void copyConstObjectToCollection(typecontainer* container, const typeobject* object)
    {
        typeobject* new_object = new typeobject();
        container->push_back( new_object );
        *new_object = *object;
    }
    
    template<typename typecontainer> inline typecontainer* createContainer()
    {
        typecontainer* new_container = new typecontainer();
        xAOD::AuxContainerBase* new_container_aux = new xAOD::AuxContainerBase();
        new_container->setStore(new_container_aux);
        return new_container;
    }
    
    inline TH1F* defineOutputHist(TString nameOutput, int nbBins=-1, double lowerBin=0, double higherBin=0)
    {
        TH1F* outputToDefine = new TH1F(nameOutput, nameOutput, nbBins, lowerBin, higherBin);
        outputToDefine->SetDirectory(out_file);
        if(DEBUG) std::cout << "Define output hist : " << outputToDefine << std::endl;
        return outputToDefine;
    }
    
    inline TTree* defineOutputTree(TString nameOutput)
    {
        TTree* outputToDefine = new TTree(nameOutput, nameOutput);
        outputToDefine->SetDirectory(out_file);
        if(DEBUG) std::cout << "Define output TTree : " << outputToDefine << std::endl;
        return outputToDefine;
    }
    
    template<typename vartype> inline void AddVarToTree(vartype& varToAdd, TString varName,TTree* treetomod = NULL)
    {
        varToAdd = vartype ();
        
        if(treetomod == NULL) treetomod = out_tree;
        treetomod->Branch(varName, &varToAdd);
    }
    
    template<typename OUTTYPE> inline OUTTYPE getConfVal(TString varname)
    {
        OUTTYPE dummy;
        
        if( !configuration->Defined(varname) ) {
            std::cout << "ERROR ! Missing configuration variable " << varname << std::endl;
            exit(1);
        }
        
        OUTTYPE res = configuration->GetValue(varname, dummy);
        
        return res;
    }
    
    
    
    
    /****************************************/
    /****************************************/
    // this is needed to distribute the algorithm to the workers
    ClassDef(TruthLevelAnalyzer, 1);
};

#endif
