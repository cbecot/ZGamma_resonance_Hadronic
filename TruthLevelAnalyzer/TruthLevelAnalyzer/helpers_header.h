#include <xAODRootAccess/tools/Message.h>

/*******************************************************/
/*******************************************************/
/*******************************************************/

#define EL_CHECK( COMMENT, EXP )                        \
  do {                                                  \
    if ( ! EXP.isSuccess() ) {                          \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),  \
             #EXP );                                    \
      return EL::StatusCode::FAILURE;                   \
    }                                                   \
  } while( false );

/*******************************************************/
/*******************************************************/
/*******************************************************/

#define CP_CHECK( COMMENT, EXP )                        \
    do {                                                  \
        if ( EXP != CP::SystematicCode::Ok ) {              \
            Error( COMMENT,                                   \
            XAOD_MESSAGE("\n  Failed to execute %s"),  \
            #EXP );                                    \
            return CP::SystematicCode::Unsupported;           \
        }                                                   \
    } while( false );

/*******************************************************/
/*******************************************************/
/*******************************************************/

#define CC_CHECK( COMMENT, EXP )                   \
  do {                                                  \
    if ( EXP != CP::CorrectionCode::Ok ) {              \
      Error( COMMENT,                                   \
             XAOD_MESSAGE("\n  Failed to execute %s"),  \
             #EXP );                                    \
    }                                                   \
  } while( false );

/*******************************************************/
/*******************************************************/
/*******************************************************/

