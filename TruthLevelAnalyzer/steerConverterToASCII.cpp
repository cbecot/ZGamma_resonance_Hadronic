//
//  steerConverterToASCII.cpp
//  
//
//  Created by cyril on 16/06/2016.
//
//

//#include "TreeToASCIIConverter.C"
//#include "TreeToASCIIConverter.h"
#include <TROOT.h>
#include <TString.h>
#include "/afs/cern.ch/work/c/cbecot/private/SearchHadZGamma/ZGamma_resonance_Hadronic/TruthLevelAnalyzer/util/libloader.C"
#include "/afs/cern.ch/work/c/cbecot/private/SearchHadZGamma/ZGamma_resonance_Hadronic/TruthLevelAnalyzer/TreeToASCIIConverter.C"


void steerConverterToASCII(TString inputfile, TString typefile="signal") // = "testTruthDxAOD0.root")
{
  //gROOT->ProcessLine(".L TreeToASCIIConverter.C++\n   TreeToASCIIConverter t(\""+inputfile+"\"); t.Loop();"
  cout << __FILE__ << " " << __LINE__ << endl;
  //TreeToASCIIConverter *t = new TreeToASCIIConverter(inputfile,typefile=="signal");
  TreeToASCIIConverter t(inputfile,typefile=="signal");
  cout << __FILE__ << " " << __LINE__ << endl;
  t.Loop();
  cout << __FILE__ << " " << __LINE__ << endl;
}
