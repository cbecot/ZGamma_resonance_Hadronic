import os

commonpath="/export/home/cbecot/Search_ZGamma_Resonace/"
commonpathsamples=commonpath+"Samples/TruthLevel/"
commonpathjos=commonpath+"jobOptions/"
commonNbEvents=50000
commonRCDirectory=commonpath+"Selectors/ZGamma_resonance_Hadronic/"

infos_oneSample = []
infos_oneSample.append({'CurrentSampleDir':commonpathsamples+"signal/mH700/", 'CurrentRunNumber':343581, 'CurrentNbEvt':commonNbEvents, 'CurrentJO':commonpathjos+"signal/mH700/MC15.343581.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH700_Zqqgam", 'CurrentOutput':commonpathsamples+"signal/mH700/output", 'RCSelectorDirectory':commonRCDirectory, 'CurrentKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_kt.env", 'CurrentAntiKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_antikt.env"})
infos_oneSample.append({'CurrentSampleDir':commonpathsamples+"signal/mH750/", 'CurrentRunNumber':343582, 'CurrentNbEvt':commonNbEvents, 'CurrentJO':commonpathjos+"signal/mH750/MC15.343582.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH750_Zqqgam", 'CurrentOutput':commonpathsamples+"signal/mH750/output", 'RCSelectorDirectory':commonRCDirectory, 'CurrentKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_kt.env", 'CurrentAntiKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_antikt.env"})
infos_oneSample.append({'CurrentSampleDir':commonpathsamples+"signal/mH800/", 'CurrentRunNumber':343583, 'CurrentNbEvt':commonNbEvents, 'CurrentJO':commonpathjos+"signal/mH800/MC15.343583.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_ggH800_Zqqgam", 'CurrentOutput':commonpathsamples+"signal/mH800/output", 'RCSelectorDirectory':commonRCDirectory, 'CurrentKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_kt.env", 'CurrentAntiKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_antikt.env"})
infos_oneSample.append({'CurrentSampleDir':commonpathsamples+"background/", 'CurrentRunNumber':361039, 'CurrentNbEvt':commonNbEvents, 'CurrentJO':commonpathjos+"background/MC15.361039.Sherpa_CT10_SinglePhoton_noPtSlice_CVetoBVeto", 'CurrentOutput':commonpathsamples+"background/output", 'RCSelectorDirectory':commonRCDirectory, 'CurrentKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_kt.env", 'CurrentAntiKTConfigDir':commonRCDirectory+"TruthLevelAnalyzer/algo_config_antikt.env"})

for currentInfoSample in infos_oneSample:
    cmdfile="/export/home/cbecot/Search_ZGamma_Resonace/productionScripts/RunnerCMDs.dat"
    currentCMDfile=open(cmdfile, 'r')
    os.system("rm -rf "+currentInfoSample['CurrentSampleDir']+"/*")
    formattedfile=open(currentInfoSample['CurrentSampleDir']+"runner.sh", 'w')
    formattedfile.write( currentCMDfile.read().format(CurrentSampleDir=currentInfoSample['CurrentSampleDir'], CurrentRunNumber=currentInfoSample['CurrentRunNumber'], CurrentNbEvt=currentInfoSample['CurrentNbEvt'], CurrentJO=currentInfoSample['CurrentJO'], CurrentOutput=currentInfoSample['CurrentOutput'], RCSelectorDirectory=currentInfoSample['RCSelectorDirectory'], CurrentKTConfigDir=currentInfoSample['CurrentKTConfigDir'], CurrentAntiKTConfigDir=currentInfoSample['CurrentKTConfigDir']) )
    formattedfile.close()
    os.system("batch < "+currentInfoSample['CurrentSampleDir']+"runner.sh")


