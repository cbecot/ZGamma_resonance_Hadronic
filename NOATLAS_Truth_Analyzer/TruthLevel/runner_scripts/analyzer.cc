#include "analyzer.h"

namespace FJ = fastjet;
using GenPart = HepMC::GenParticle;
using GenEvt = HepMC::GenEvent;


/********************************************************/
/********************************************************/
/********************************************************/


int main(int argc, char **argv)
{
    
    /********************************************************/
    /***		 Check the presence of arguments 	***/
    //@{
  string input_file_name, output_file_name, config_file_name, output_asciifile_name;
    TEnv configuration;
    double initialJetRadius, PtJetMin_forFastJet, PtJetMax, PtJetMin, EtaJetMax;
    unsigned int printeveryevt, printNJets, DEBUG_LVL;
    bool DEBUG;
    
    if(argc < 4) {
        cerr << "Not enough arguments provided ! " << argc << endl;
        cerr << "--- arg1 = input HepMC file" << endl;
        cerr << "--- arg2 = output root file" << endl;
        cerr << "--- arg3 = configuration file" << endl;
        return 0;
    } else {
        cout << "************************************************" << endl;
        cout << "Will run on the following inputs :" << endl;
        for(size_t idarg = 0 ; idarg < argc ; idarg++) cout << argv[idarg] << endl;
        cout << "************************************************" << endl << endl << endl << endl;
        
        input_file_name = string(argv[1]);
        output_file_name = string(argv[2]);
	output_file_name += "_wjcorr";
	output_asciifile_name = output_file_name+".dat";
        config_file_name = string(argv[3]);

        configuration.ReadFile(config_file_name.data(), EEnvLevel::kEnvAll);
        cout << "************************************************" << endl;
        cout << "With the following configuration :" << endl;
        cout << "************************************************" << endl;
        configuration.Print();
        
        initialJetRadius = configuration.GetValue("initialJetRadius", 1.0);
        PtJetMin_forFastJet = configuration.GetValue("PtJetMin_forFastJet", 20.0);
        PtJetMax = configuration.GetValue("PtJetMax", 1000.0);
        PtJetMin = configuration.GetValue("PtJetMin", 200.0);
        EtaJetMax = configuration.GetValue("EtaJetMax", 2.5);
        printeveryevt = configuration.GetValue("printeveryevt", 100);
        printNJets = configuration.GetValue("printNJets", 10);
        DEBUG = configuration.GetValue("DEBUG", false);
        DEBUG_LVL = configuration.GetValue("DEBUG_LVL", 1);
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " DEBUG_LVL   " << DEBUG_LVL << endl;
        
        cout << "************************************************" << endl << endl << endl << endl;
    }
    /********************************************************/

    ofstream output_asciifile;
    output_asciifile.open(output_asciifile_name);
    output_asciifile << std::scientific;
    output_asciifile << "###########################################" << endl;
    output_asciifile << "###     WILL BE FORMATED AS FOLLOWS :   ###" << endl;
    output_asciifile << "### NEEVT: <nb of hadrons> <nb of jets> <jet radius> ###" << endl;
    output_asciifile << "### HADS: [Pt, Eta, Phi, E] (contain all -interacting!- final state particles in event) ###" << endl;
    output_asciifile << "### NEJET: <mothers of partons in jet> [Pt, Eta, Phi, E] (for all hads in evt) ###" << endl;
    output_asciifile << "###########################################" << endl;


    

    /*** Retrieve the input file and its content ***/
    HepMC::IO_GenEvent input_file(input_file_name, ios::in);
    if (input_file.rdstate() == ios::failbit) {
        cerr << "ERROR input file " << input_file_name << " is needed and does not exist." << std::endl;
        return 1;
    } else {
        cout << "File " << input_file_name << " is open and ready to be run on." << endl;
    }
    //@}


    
    /*************************************************/
    /***		 Define the outputs		 ***/
    //@{
    TFile* outfile = new TFile(output_file_name.data(), "RECREATE");
    
    TH1F* h_njets = new TH1F("h_njets", "h_njets", 10, 0, 10);
    TH1F* h_cutflow = new TH1F("h_cutflow", "h_cutflow", 100, 0, 100);
    vector<TH1F*> h_pt_jet, h_eta_jet, h_phi_jet;
    for(unsigned int id = 0 ; id < printNJets ; id++) {
        TString name = "_jet"+TString::Itoa(id+1, 10);
        h_pt_jet.push_back( new TH1F("h_pt"+name, "h_pt"+name, 80, 200, 1000) );
        h_eta_jet.push_back( new TH1F("h_eta"+name, "h_eta"+name, 400, -2.5, 2.5) );
        h_phi_jet.push_back( new TH1F("h_phi"+name, "h_phi"+name, 64, -3.2, 3.2) );
    }
    
    TTree* t_output = new TTree("outtree", "outtree");
    handle_output OutputHandler(t_output, initialJetRadius);
    //@}
    
    
    /************************************************/
    /*** 		Start reading the file 		***/
    unsigned int count_events = 0;
    IsFinalState check_isFS;
    
    GenEvt current_event;
    while(input_file.fill_next_event(&current_event))
    {
      /*if(count_events > 60500) {
	DEBUG = true;
	DEBUG_LVL = 999999;
      } else {
	if(count_events%10000 == 0) cout << "Event nb#" << count_events << endl;
	++count_events;
	continue;
	}*/
        current_event.use_units(HepMC::Units::GEV, HepMC::Units::MM);
        if(DEBUG && (DEBUG_LVL > 3) ) {
            cout << endl << endl << endl << endl << endl;
            cout << "***** NEW EVENT ! *****" << endl;
            current_event.print();
        }
        
        
        /***/
        if(DEBUG && (DEBUG_LVL > 150) ) {
            GenEvt::particle_iterator tmp_current_particle = current_event.particles_begin();
            GenEvt::particle_iterator tmp_part_e = current_event.particles_end();
            
            for( ; tmp_current_particle != tmp_part_e ; ++tmp_current_particle)
            {
                if( !check_isFS(*tmp_current_particle) ) continue;
                vector<GenPart*> parents = MCUtils::findParents(*tmp_current_particle);
                cout << "Parent gen part = " << parents[0] << endl;
                if(parents[0] != NULL) parents[0]->print();
                break;
            }
            
            break;
        }
        /***/
        
        
        ++count_events;
        if(count_events%printeveryevt == 0)
        cout << "_____ Processing event nb" << count_events << " _____" << endl;
        vector<GenPart*> allFSparticles, allNonBeamPartons, allFSPartons, allFSHadrons, allFSPhotons, allFSChargedLeptons;
        GenPart* zboson = NULL;
        
        
        /***** Loop on all particles to recover the interesting ones *****/
        GenEvt::particle_iterator current_particle = current_event.particles_begin();
        GenEvt::particle_iterator part_e = current_event.particles_end();
        for( ; current_particle != part_e ; ++current_particle)
        {
            if(DEBUG && (DEBUG_LVL > 9) ) (*current_particle)->print();
            
            IsParticleType check_particleType(*current_particle);
            if(!check_particleType.properly_retrieved) {
                cerr << "Could not retrieve one particle !" << endl;
                return -1;
            }
            
            if( check_particleType.Zboson() ) zboson = *current_particle;
            
            if( (*current_particle)->is_beam() ) continue;
            
            if(check_particleType.parton())
            allNonBeamPartons.push_back(*current_particle);
            
            if( !check_isFS(*current_particle) ) continue;
            
            allFSparticles.push_back(*current_particle);
            if(check_particleType.parton()) allFSPartons.push_back(*current_particle);
            else if(check_particleType.hadron()) allFSHadrons.push_back(*current_particle);
            else if(check_particleType.photon()) allFSPhotons.push_back(*current_particle);
	    else if(check_particleType.charged_lepton()) allFSChargedLeptons.push_back(*current_particle);
        } //*** End loop on particles ***
        
        if(DEBUG) cout << "Found " << allFSHadrons.size() << " FS Hadrons, as well as " << allNonBeamPartons.size() << " partons." << endl;
        
        if(zboson != NULL) {
            if(DEBUG) cout << "Found one Z boson" << endl;
            IsParticleType check_particleType(MCUtils::findChildren(zboson)[0]);
            if( !check_particleType.parton() ) {
                if(DEBUG) cout << "Decay into lepton are removed" << endl;
                continue;
            }
        } else {
            if(DEBUG) cout << "No Z boson in this event" << endl;
        }
        
        
        /***** Build the jets *****/
        vector<FJ::PseudoJet> allFSHadronsPseudoJets;
        for(auto current = allFSHadrons.begin() ; current != allFSHadrons.end() ; ++current) {
            double px = (*current)->momentum().px();
            double py = (*current)->momentum().py();
            double pz = (*current)->momentum().pz();
            double e  = (*current)->momentum().e();
            allFSHadronsPseudoJets.push_back( FJ::PseudoJet(px, py, pz, e) );
        }
	for(auto current = allFSPhotons.begin() ; current != allFSPhotons.end() ; ++current) {
	  double px = (*current)->momentum().px();
	  double py = (*current)->momentum().py();
	  double pz = (*current)->momentum().pz();
	  double e  = (*current)->momentum().e();
	  allFSHadronsPseudoJets.push_back( FJ::PseudoJet(px, py, pz, e) );
        }
	for(auto current = allFSChargedLeptons.begin() ; current != allFSChargedLeptons.end() ; ++current) {
          double px = (*current)->momentum().px();
          double py = (*current)->momentum().py();
          double pz = (*current)->momentum().pz();
          double e  = (*current)->momentum().e();
          allFSHadronsPseudoJets.push_back( FJ::PseudoJet(px, py, pz, e) );
        }
        
        FJ::JetDefinition InitialJetDef(FJ::JetAlgorithm::antikt_algorithm, initialJetRadius);
        FJ::ClusterSequence InitialClusterSeq(allFSHadronsPseudoJets, InitialJetDef);
        vector<FJ::PseudoJet> InitialFatJets = FJ::sorted_by_pt(InitialClusterSeq.inclusive_jets(PtJetMin_forFastJet));
        
        if(DEBUG) {
            cout << "Found the following " << InitialFatJets.size() << " jets" << endl;
            for(auto currentjet : InitialFatJets)
                cout << currentjet.Et() << " " << currentjet.phi() << " " << currentjet.pseudorapidity() << endl;
        }
        
        //PtJetMax, PtJetMin, EtaJetMax        
        /*h_cutflow->Fill(0.5);
        if( (InitialFatJets[0].Et() < PtJetMin) || (InitialFatJets[0].Et() > PtJetMax) ) {
            if((InitialFatJets[1].Et() < PtJetMin)||(InitialFatJets[1].Et() > PtJetMax))
                continue;
        }
        h_cutflow->Fill(1.5);
        if (fabs(InitialFatJets[0].pseudorapidity()) > EtaJetMax) {
            if (fabs(InitialFatJets[1].pseudorapidity()) > EtaJetMax) continue;
        }
        h_cutflow->Fill(2.5);*/
        
        /***** Find the input partons for each of the jets *****/
        int nbFatJet = InitialFatJets.size();
        vector<int> nbMatchingPartons(nbFatJet, 0);
        vector< vector<GenPart*> > matchingPartons( nbFatJet, vector<GenPart*>() );
        
        for(int id_fatjet = 0 ; id_fatjet < nbFatJet ; id_fatjet++)
        for(int idparton = 0 ; idparton < allNonBeamPartons.size() ; idparton++) {
            if(DeltaR(InitialFatJets.at(id_fatjet), allNonBeamPartons.at(idparton)) < initialJetRadius)
            matchingPartons.at(id_fatjet).push_back(allNonBeamPartons.at(idparton));
        }
        
        
        /***** Now find the origin of these partons *****/
        vector<count_parton_origin> count_origins(nbFatJet, count_parton_origin(DEBUG, DEBUG_LVL));
        for(int id_fatjet = 0 ; id_fatjet < nbFatJet ; id_fatjet++) {
            int nbPartons = matchingPartons.at(id_fatjet).size();
            if(DEBUG) cout << __LINE__ << " Will run on " << nbPartons << " partons" << endl;
            for(size_t idparton = 0 ; idparton < nbPartons ; idparton++)
            count_origins.at(id_fatjet).add_count(matchingPartons.at(id_fatjet).at(idparton));
            if(DEBUG) cout << count_origins.at(id_fatjet).dump_content() << endl;
        }
        
        
        /************************************************/
        /**********   Now fill the outputs !   **********/
        //@{
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling event weights " << current_event.weights().size() << " " << endl;
        HepMC::WeightContainer current_event_weights = current_event.weights();
        for(int id = 0 ; id < current_event_weights.size() ; id++) {
            OutputHandler.m_weights.push_back( current_event_weights[id] );
            if(DEBUG && (DEBUG_LVL == 20) )
            cout << __FILE__ << " " << __LINE__ << " wei = " << OutputHandler.m_weights.back() << endl;
        }
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling fat jets " << nbFatJet << " " << InitialFatJets.size() << " " << h_pt_jet.size() << endl;
        h_njets->Fill(nbFatJet);
        for(int id_fatjet = 0 ; id_fatjet < nbFatJet ; id_fatjet++) {
	  double pt = InitialFatJets.at(id_fatjet).Et(), eta = InitialFatJets.at(id_fatjet).pseudorapidity(), phi = InitialFatJets.at(id_fatjet).phi_std(), e = InitialFatJets.at(id_fatjet).E();
	  double px = InitialFatJets.at(id_fatjet).px(), py = InitialFatJets.at(id_fatjet).py(), pz = InitialFatJets.at(id_fatjet).pz(), ene = InitialFatJets.at(id_fatjet).e();
            
            if(id_fatjet < printNJets) {
                h_pt_jet.at(id_fatjet)->Fill(pt);
                h_eta_jet.at(id_fatjet)->Fill(eta);
                h_phi_jet.at(id_fatjet)->Fill(phi);
            }
            
            OutputHandler.m_jet4v_antikt.push_back( LV() );
            OutputHandler.m_jet4v_antikt.back().SetPxPyPzE(px,py,pz,ene);
            
            OutputHandler.m_jet_constituents.push_back( vector<LV> () );
            for(auto tmpConst4v : InitialFatJets.at(id_fatjet).constituents()) {
                OutputHandler.m_jet_constituents.back().push_back( LV() ); OutputHandler.m_jet_constituents.back().back().SetPtEtaPhiE(tmpConst4v.Et(),tmpConst4v.eta(),tmpConst4v.phi_std(),tmpConst4v.E());
            }
        }
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling Z boson" << endl;
        if(zboson != NULL)
        {
            OutputHandler.m_z4v.SetPxPyPzE(zboson->momentum().px(), zboson->momentum().py(),
                                           zboson->momentum().pz(), zboson->momentum().e());
            vector<GenPart*> tmpZdaughters = MCUtils::findChildren(zboson);
            for(int id = 0 ; id < tmpZdaughters.size() ; id++) {
                HepMC::FourVector t = tmpZdaughters[id]->momentum();
                OutputHandler.m_Zdaughters.push_back( LV() );
                OutputHandler.m_Zdaughters.back().SetPxPyPzE(t.px(), t.py(), t.pz(), t.e());
                OutputHandler.m_Zdaughters_PID.push_back(tmpZdaughters[id]->pdg_id());
            }
        }
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling hadrons" << endl;
        for(auto hadron4v : allFSHadronsPseudoJets) {
            OutputHandler.m_hadron4v.push_back( LV() );
            OutputHandler.m_hadron4v.back().SetPxPyPzE(hadron4v.px(),hadron4v.py(),hadron4v.pz(),hadron4v.e());
        }
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling partons" << endl;
        for(auto parton4v : allNonBeamPartons) {
            OutputHandler.m_parton4v.push_back( LV() );
            HepMC::FourVector tmp4v = parton4v->momentum();
            OutputHandler.m_parton4v.back().SetPxPyPzE(tmp4v.px(), tmp4v.py(), tmp4v.pz(), tmp4v.e());
        }
        sort(OutputHandler.m_parton4v.begin(), OutputHandler.m_parton4v.end(), HasGreaterPT);
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling parton origins" << endl;
        for(auto tmp_origins : count_origins)
        OutputHandler.m_jets_initParticule_mother_PDGIDs.push_back(tmp_origins.dump_content());
        //---------
        
        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling photons" << endl;
        for(auto currentph : allFSPhotons) {
            OutputHandler.m_photon4v.push_back( LV() );
            OutputHandler.m_photon4v.back().SetPxPyPzE(currentph->momentum().px(), currentph->momentum().py(), currentph->momentum().pz(), currentph->momentum().e());
        }
        sort(OutputHandler.m_photon4v.begin(), OutputHandler.m_photon4v.end(), HasGreaterPT);
        //---------
        
        if(OutputHandler.m_parton4v.size() > 1) OutputHandler.m_jjg4v = OutputHandler.m_photon4v[0] + OutputHandler.m_parton4v[0] + OutputHandler.m_parton4v[1];
        //---------
        //@}
        /************************************************/
        /************************************************/
        /*    output_asciifile << "### NEEVT: <nb of hadrons> <nb of jets> <jet radius> <weight to match pt dist> <weight to match eta dist> ###" << endl;
	      output_asciifile << "### HADS: [Pt, Eta, Phi, E] (contain all hadrons in event) ###" << endl;
	      output_asciifile << "### NEJET: <mothers of partons in jet> [Pt, Eta, Phi, E] (for all hads in evt) ###" << endl;*/

	output_asciifile << endl << endl << endl << endl;
	output_asciifile << "NEEVT : " << OutputHandler.m_hadron4v.size() << " " << OutputHandler.m_jet4v_antikt.size() << " " << initialJetRadius << endl;
	output_asciifile << "HADS:"; 
	for (auto current4v : OutputHandler.m_hadron4v) output_asciifile << " [" << current4v.Pt() << "," << current4v.Eta() << "," << current4v.Phi() << "," << current4v.E() << "]";
	output_asciifile << endl;
	for(size_t idjet = 0 ; idjet < OutputHandler.m_jet4v_antikt.size() ; idjet++) {
	  auto current4v = OutputHandler.m_jet4v_antikt.at(idjet);
	  output_asciifile << "NEJET: " << OutputHandler.m_jets_initParticule_mother_PDGIDs.at(idjet) << "[" << current4v.Pt() << "," << current4v.Eta() << "," << current4v.Phi() << "," << current4v.E() << "] ";
	  output_asciifile << endl;
	}

        if(DEBUG) cout << __FILE__ << " " << __LINE__ << " Now filling the TTree" << endl;
        OutputHandler.fill();
        OutputHandler.clear_vectors();
    } //*** End loop on events ***
    

    outfile->Write();
    cout << "Ran on " << count_events << " events. " << endl; 
    output_asciifile << endl << endl << endl << "### RAN ON " << count_events << " EVENTS ###" << endl;
    output_asciifile << "###########################################" << endl;
    output_asciifile << "###########################################" << endl;
    output_asciifile << "###########################################" << endl;
    output_asciifile.close();
    /************************************************/
}


/********************************************************/
/********************************************************/
/********************************************************/
