#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -l walltime=20:00:00
#PBS -l mem=4GB
#PBS -N redo_analysis_2
#PBS -M cb3605@nyu.edu
#PBS -j oe

module purge
module load gcc/4.9.2
source /home/cb3605/root/bin/thisroot.sh
LD_LIBRARY_PATH=/share/apps/gcc/4.9.2/lib64:/share/apps/gcc/4.9.2/lib:/home/cb3605/root/lib:/home/cb3605/jetanalyzer/fastjet-3.2.1-build/lib:$LD_LIBRARY_PATH

${init_dir}/analyzer.exe /scratch/cb3605/MyArchiveCopy/dijet.hepmc /scratch/cb3605/MyArchiveCopy/dijet_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
${init_dir}/analyzer.exe /scratch/cb3605/MyArchiveCopy/wprime.hepmc /scratch/cb3605/MyArchiveCopy/wprime_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
${init_dir}/analyzer.exe /scratch/cb3605/MyArchiveCopy/zprime.hepmc /scratch/cb3605/MyArchiveCopy/zprime_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env

#${init_dir}/analyzer.exe /work/cb3605/HepMCprod/dijet_v2.hepmc /work/cb3605/HepMCprod/dijet_v2_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
#${init_dir}/analyzer.exe /work/cb3605/HepMCprod/wprime_v2.hepmc /work/cb3605/HepMCprod/wprime_v2_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
#${init_dir}/analyzer.exe /work/cb3605/HepMCprod/zprime_v2.hepmc /work/cb3605/HepMCprod/zprime_v2_wjcorr.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env

#/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /work/cb3605/HepMCprod/dijet_delphes_v2.root /work/cb3605/HepMCprod/dijet_delphes_v2_analyzed_wPFlow.dat /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
#/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /work/cb3605/HepMCprod/wprime_delphes_v2.root /work/cb3605/HepMCprod/wprime_delphes_v2_analyzed_wPFlow.dat /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
#/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /work/cb3605/HepMCprod/zprime_delphes_v2.root /work/cb3605/HepMCprod/zprime_delphes_v2_analyzed_wPFlow.dat /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env

/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /scratch/cb3605/MyArchiveCopy/dijet_delphes.root /scratch/cb3605/MyArchiveCopy/dijet_delphes_analyzed_wPFlow.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /scratch/cb3605/MyArchiveCopy/wprime_delphes.root /scratch/cb3605/MyArchiveCopy/wprime_delphes_analyzed_wPFlow.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env
/home/cb3605/jetanalyzer/delphes/Example1_withPFlow /scratch/cb3605/MyArchiveCopy/zprime_delphes.root /scratch/cb3605/MyArchiveCopy/zprime_delphes_analyzed_wPFlow.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env


