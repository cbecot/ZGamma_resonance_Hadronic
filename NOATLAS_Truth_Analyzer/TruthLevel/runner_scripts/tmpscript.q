#!/bin/bash

#PBS -l nodes=1:ppn=1
#PBS -l walltime=20:00:00
#PBS -l mem=4GB
#PBS -N redo_analysis
#PBS -M cb3605@nyu.edu
#PBS -j oe

module purge
module load gcc/4.9.2
source /home/cb3605/root/bin/thisroot.sh
LD_LIBRARY_PATH=/share/apps/gcc/4.9.2/lib64:/share/apps/gcc/4.9.2/lib:/home/cb3605/root/lib:/home/cb3605/jetanalyzer/fastjet-3.2.1-build/lib:$LD_LIBRARY_PATH

truth_ana_dir = /home/cb3605/home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel
delphes_ana_dir = /home/cb3605/jetanalyzer/delphes

${truth_ana_dir}/runner_scripts/analyzer.exe /archive/cb3605/dijet.hepmc /archive/cb3605/dijet_wjcorr.root ${truth_ana_dir}/analyzer_config.env
${truth_ana_dir}/runner_scripts/analyzer.exe /archive/cb3605/wprime.hepmc /archive/cb3605/wprime_wjcorr.root ${truth_ana_dir}/analyzer_config.env
${truth_ana_dir}/runner_scripts/analyzer.exe /archive/cb3605/zprime.hepmc /archive/cb3605/zprime_wjcorr.root ${truth_ana_dir}/analyzer_config.env

${truth_ana_dir}/runner_scripts/analyzer.exe /work/cb3605/HepMCprod/dijet_v2.hepmc /work/cb3605/HepMCprod/dijet_v2_wjcorr.root ${truth_ana_dir}/analyzer_config.env
${truth_ana_dir}/runner_scripts/analyzer.exe /work/cb3605/HepMCprod/wprime_v2.hepmc /work/cb3605/HepMCprod/wprime_v2_wjcorr.root ${truth_ana_dir}/analyzer_config.env
${truth_ana_dir}/runner_scripts/analyzer.exe /work/cb3605/HepMCprod/zprime_v2.hepmc /work/cb3605/HepMCprod/zprime_v2_wjcorr.root ${truth_ana_dir}/analyzer_config.env

${delphes_ana_dir}/Example1_withPFlow /work/cb3605/HepMCprod/dijet_delphes_v2.root /work/cb3605/HepMCprod/dijet_delphes_v2_analyzed_wPFlow.dat ${truth_ana_dir}/analyzer_config.env
${delphes_ana_dir}/Example1_withPFlow /work/cb3605/HepMCprod/wprime_delphes_v2.root /work/cb3605/HepMCprod/wprime_delphes_v2_analyzed_wPFlow.dat ${truth_ana_dir}/analyzer_config.env
${delphes_ana_dir}/Example1_withPFlow /work/cb3605/HepMCprod/zprime_delphes_v2.root /work/cb3605/HepMCprod/zprime_delphes_v2_analyzed_wPFlow.dat ${truth_ana_dir}/analyzer_config.env

${delphes_ana_dir}/Example1_withPFlow /archive/cb3605/dijet_delphes.root /archive/cb3605/dijet_delphes_analyzed_wPFlow.root ${truth_ana_dir}/analyzer_config.env
${delphes_ana_dir}/Example1_withPFlow /archive/cb3605/wprime_delphes.root /archive/cb3605/wprime_delphes_analyzed_wPFlow.root ${truth_ana_dir}/analyzer_config.env
${delphes_ana_dir}/Example1_withPFlow /archive/cb3605/zprime_delphes.root /archive/cb3605/zprime_delphes_analyzed_wPFlow.root ${truth_ana_dir}/analyzer_config.env


