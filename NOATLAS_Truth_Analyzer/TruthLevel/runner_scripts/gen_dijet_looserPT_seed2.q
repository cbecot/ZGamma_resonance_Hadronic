#!/bin/bash                                                                                                                                                                 
#PBS -l nodes=1:ppn=1                                                                                                                                                      
#PBS -l walltime=20:00:00                                                                                                                                                 
#PBS -l mem=4GB                                                                                                                                                            
#PBS -N gen_dijet_pythia                                                                                                                                    
#PBS -M cb3605@nyu.edu                                                                                                                                      
#PBS -j oe                                                                                                                                                                  
module purge
module load gcc/4.9.2
LD_LIBRARY_PATH=/share/apps/gcc/4.9.2/lib64:/share/apps/gcc/4.9.2/lib:home/cb3605/root/lib:/home/cb3605/jetanalyzer/fastjet-3.2.1-build/lib:$LD_LIBRARY_PATH

SEED=2
processname=dijet_looserPT_seed${SEED}

cd /scratch/cb3605/
/work/cb3605/pythia8219/examples/main42 /work/cb3605/HepMCprod/main42_${processname}.cmnd ${PWD}/${processname}_v2.hepmc

source /home/cb3605/root/bin/thisroot.sh
/home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer.exe ${PWD}/${processname}_v2.hepmc ${processname}_v2.root /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env

/home/cb3605/jetanalyzer/delphes/DelphesHepMC /home/cb3605/jetanalyzer/delphes/cards/delphes_card_ATLAS.tcl ${PWD}/${processname}_delphes_v2.root ${PWD}/${processname}_v2.hepmc
/home/cb3605/jetanalyzer/delphes/Example1 ${PWD}/${processname}_delphes_v2.root ${PWD}/${processname}_delphes_analyzed_v2.dat /home/cb3605/jetanalyzer/ZGamma_resonance_Hadronic/NOATLAS_Truth_Analyzer/TruthLevel/analyzer_config.env







