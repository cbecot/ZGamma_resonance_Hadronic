#include <MCUtils/PIDUtils.h>
#include <MCUtils/HepMCParticleUtils.h>

#include <HepMC/IO_GenEvent.h>
#include <HepMC/GenEvent.h>
#include <HepMC/GenParticle.h>

#include <fastjet/PseudoJet.hh>
#include <fastjet/JetDefinition.hh>
#include <fastjet/ClusterSequence.hh>

#include <TTree.h>
#include <TEnv.h>
#include <TH1F.h>
#include <TFile.h>
#include <TROOT.h>
#include <TLorentzVector.h>
#include <Math/GenVector/LorentzVector.h>

#include <math.h>
#include <algorithm>
#include <iostream>
#include <string>
#include <list>

using namespace std;
using LV = TLorentzVector; 
//::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double> >;


/***************************************************/
/***************************************************/


bool  HasGreaterPT(LV a, LV b) { return (a.Pt() > b.Pt()); };


/***************************************************/
/***************************************************/


template<typename vartype> inline void AddVarToTree(vartype& varToAdd, TString varName,TTree* treetomod = NULL)
{
    varToAdd = vartype ();
        
    treetomod->Branch(varName, &varToAdd);
}


/***************************************************/
/***************************************************/


double DeltaR(fastjet::PseudoJet& jet, HepMC::GenParticle* parton)
{
  TLorentzVector parton4v, jet4v;
  parton4v.SetPxPyPzE(parton->momentum().px(), parton->momentum().py(), 
		      parton->momentum().pz(), parton->momentum().e());
  jet4v.SetPxPyPzE(jet.px(), jet.py(), 
		   jet.pz(), jet.e());
  
  double result = parton4v.DeltaR(jet4v);
  
  return result;
}


/***************************************************/
/***************************************************/


class IsFinalState {
public:
    /// returns true if the GenParticle does not decay                                                                                                                      
    bool operator()( const HepMC::GenParticle* p ) {
        if ( !p->end_vertex() && p->status()==1 ) return true;
        return false;
    }
};


/***************************************************/
/***************************************************/


class IsParticleType {
public:
  
  bool properly_retrieved;
  
  bool Zboson() { return MCUtils::PID::isZ(current_pdgid); }
  bool Wplus() { return MCUtils::PID::isWplus(current_pdgid); }
  bool Wminus() { return MCUtils::PID::isWminus(current_pdgid); }
  bool Wboson() { return (Wplus() ||  Wminus()); }
  bool parton() { return MCUtils::PID::isParton(current_pdgid); }
  bool charged_lepton() { return MCUtils::PID::isChargedLepton(current_pdgid); }
  bool hadron() { return MCUtils::PID::isHadron(current_pdgid); }
  bool top() { return MCUtils::PID::isTop(current_pdgid); }
  bool photon() { return MCUtils::PID::isPhoton(current_pdgid); }
  bool gluon() { return MCUtils::PID::isGluon(current_pdgid); }
  bool quark() { return MCUtils::PID::isQuark(current_pdgid); }
  bool higgs() { return MCUtils::PID::isHiggs(current_pdgid); }
  
  bool set_particle( HepMC::GenParticle* p ) {
    if(p == NULL) return false;
    current_particle = p;
    return true;
  }
  
  IsParticleType() { current_particle = NULL; };
  IsParticleType(int tmp_pdgid) { current_pdgid = tmp_pdgid; };
  IsParticleType(HepMC::GenParticle* p) {
    properly_retrieved = set_particle(p);
    current_pdgid = current_particle->pdg_id();
  };
  
  
private:
  
  HepMC::GenParticle* current_particle;
  int current_pdgid;
};


/***************************************************/
/***************************************************/


class count_parton_origin {
public:
  
  count_parton_origin(bool dbg, int dbglvl) {
    from_Z = 0;
    from_Wp = 0;
    from_Wm = 0;
    from_W = 0;
    from_top = 0;
    from_photon = 0;
    from_gluon = 0;
    from_quark = 0;
    from_higgs = 0;
    others = 0;
    moreThanOneMothers = 0;
    noParents = 0;
    /*zeroPartAtVertex = 0;
    noPV = 0;*/
    
    DEBUG = dbg;
    DEBUG_LVL = dbglvl;
    
    already_found_particles = list<HepMC::GenParticle*> ();
  };
  
  void add_count(HepMC::GenParticle* current_particle) {
    if(DEBUG) current_particle->print();
    
    list<HepMC::GenParticle*>::iterator test_end = already_found_particles.end();
    HepMC::GenParticle* test_particle = MCUtils::findFirstReplica(current_particle);
    if( find(already_found_particles.begin(), test_end, test_particle) != test_end ) return;
    already_found_particles.push_back(test_particle);
    if(DEBUG) {
      cout << __FILE__ << " " << __LINE__ << endl;
      for(auto current : already_found_particles) cout << "pid = " << current << " ; ";
      cout << endl;
    }
    
    
    vector<HepMC::GenParticle*> parents = MCUtils::findParents(current_particle);
    if(parents.size() == 0) ++noParents;
    else if(parents.size() > 1) ++moreThanOneMothers;
    if(parents.size() != 1) return;
    /*
    HepMC::GenVertex* initVertex = current_particle.production_vertex();
    if(initVertex == NULL) {
      ++noPV;
      return;
    }
    if(DEBUG) cout << "initVertex " << initVertex << endl;
    
    if(initVertex->particles_in_size() == 0)
      ++zeroPartAtVertex;
    else if(initVertex->particles_in_size() > 1) {
      ++moreThanOneMothers;
      return;
    }
    */
    
    int current_pdgid = parents.at(0)->pdg_id();
    //(*(initVertex->particles_out_const_begin()))->pdg_id();
    
    IsParticleType checkPartType(current_pdgid);
    
    if(checkPartType.Zboson()) {
      ++from_Z;
      if(DEBUG && (DEBUG_LVL > 2) ) {
	cout << "Here is the Z boson : " << endl;
	parents.at(0)->print();
	cout << "Its production vertex : " << endl;
	parents.at(0)->production_vertex()->print();
	cout << "Its decay vertex : " << endl;
	parents.at(0)->end_vertex()->print();
      }
    } else if (checkPartType.top()) {
      ++from_top;
    } else if(checkPartType.Wplus()) {
      ++from_Wp;
      ++from_W;
    } else if(checkPartType.Wminus()) {
      ++from_Wm;
      ++from_W;
    } else if(checkPartType.photon()) {
      ++from_photon;
    } else if(checkPartType.gluon()) {
      ++from_gluon;
    } else if(checkPartType.quark()) {
      ++from_quark;
    } else if(checkPartType.higgs()) {
      ++from_higgs;
    } else {
      ++others;
    }
    
  };
  
  string dump_content()
  {
    string result = "";
    result += "Z:"+to_string(from_Z)+",";
    result += "Wp:"+to_string(from_Wp)+",";
    result += "Wm:"+to_string(from_Wm)+",";
    result += "W:"+to_string(from_W)+",";
    result += "top:"+to_string(from_top)+",";
    result += "others:"+to_string(others)+",";
    result += "photon:"+to_string(from_photon)+",";
    result += "gluon:"+to_string(from_gluon)+",";
    result += "quark:"+to_string(from_quark)+",";
    result += "higgs:"+to_string(from_higgs)+",";
    result += "moreThanOneMothers:"+to_string(moreThanOneMothers)+",";
    result += "noParents:"+to_string(noParents);
    /*result += "zeroPartAtVertex:"+to_string(zeroPartAtVertex)+",";
    result += "noPV:"+to_string(noPV);*/
    
    return result;
  };
  
  int from_Z;
  int from_Wp;
  int from_Wm;
  int from_W;
  int from_top;
  int others;
  int moreThanOneMothers;
  int from_photon;
  int from_gluon;
  int from_quark;
  int from_higgs;
  int noParents;
  /*int zeroPartAtVertex;
  int noPV;*/
  
  list<HepMC::GenParticle*> already_found_particles;
  
  bool DEBUG;
  int DEBUG_LVL;
  
};


/***************************************************/
/***************************************************/


class handle_output {
public:
  
  handle_output() { 
    handle_output(NULL, 1.0);
  };
  
  handle_output(TTree* tmptree, double current_jet_radius) { 
    outtree = tmptree;  
    setup_vectors(current_jet_radius);
    firstEvt = true;
  }; 
  
  ~handle_output() {
    clear_vectors();
    outtree->Delete();
  };
  
  void fill() {
    outtree->Fill();
  };
  
  void setup_vectors(double current_jet_radius) { 
    if(outtree == NULL) {
      cerr << "Warning ! Output tree not properly defined" <<endl;
      exit(1);
    }
    
    AddVarToTree< vector<LV> > (m_Zdaughters, "Zdaughters", outtree); 
    AddVarToTree< vector<LV> > (m_jet4v_antikt, "jet4v_antikt", outtree);
    AddVarToTree< vector<LV> > (m_hadron4v, "hadron4v", outtree); 
    AddVarToTree< vector<LV> > (m_parton4v, "parton4v", outtree); 
    AddVarToTree< LV > (m_z4v, "z4v", outtree); 
    AddVarToTree< vector<LV> > (m_photon4v, "photon4v", outtree);
    AddVarToTree< LV > (m_jjg4v, "jjg4v", outtree); 
    AddVarToTree< vector< vector<LV> > > (m_jet_constituents, "jet_constituents", outtree);   
    AddVarToTree< vector<string> > (m_jets_initParticule_mother_PDGIDs, "jets_initParticule_mother_PDGIDs", outtree);
    
    AddVarToTree<double> (m_jet_radius, "jet_radius", outtree);  m_jet_radius= current_jet_radius; 
    AddVarToTree< vector<double> >(m_weights, "weights", outtree);
    AddVarToTree< vector<int> > (m_Zdaughters_PID, "Zdaughters_PID", outtree);
  };
  
  void clear_vectors() {
    if(firstEvt == true) {
      firstEvt = false;
      return;
    }
    m_Zdaughters.clear(); 
    m_jet4v_antikt.clear(); 
    m_hadron4v.clear(); 
    m_parton4v.clear(); 
    m_z4v = LV(); 
    m_photon4v.clear();
    m_jjg4v = LV(); 
    m_jet_constituents.clear(); 
    m_jets_initParticule_mother_PDGIDs.clear(); 
    m_weights.clear();
    m_Zdaughters_PID.clear();
  };
  
  TTree* outtree; 
  bool firstEvt;
  
  vector<LV> m_Zdaughters; //
  vector<int> m_Zdaughters_PID; //
  LV m_z4v; //
  vector<LV> m_jet4v_antikt; //
  double m_jet_radius; //
  vector<LV> m_hadron4v; //
  vector<LV> m_parton4v; //
  vector<string> m_jets_initParticule_mother_PDGIDs; //
  vector< vector<LV> > m_jet_constituents; //
  vector<LV> m_photon4v; //
  LV m_jjg4v; //
  vector<double> m_weights;
  
};


/***************************************************/
/***************************************************/
