//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Nov  9 10:28:35 2016 by ROOT version 6.04/14
// from TTree outtree/outtree
// found on file: /Users/cyril/Travail/DockerImages/MG5GRIDGenerations/test_zgam_ptjmax500_ptj200/Background_output_events/analyzed_truthlvl_1.root
//////////////////////////////////////////////////////////

#ifndef dump_into_ascii_h
#define dump_into_ascii_h

#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TLegend.h>
#include <fstream>

// Header file for the classes stored in the TTree if any.
#include "TLorentzVector.h"
#include <vector>
#include <vector>
#include <vector>

#define DEBUG true

using namespace std;

class dump_into_ascii {
    public :
    TTree          *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t           fCurrent; //!current Tree number in a TChain
    TString         outputASCIIFile;
    TString         inputASCIIFile;
    TFile          *file;
    bool            is_signal;
    
    // Fixed size dimensions of array or collections stored in the TTree if any.
    
    // Declaration of leaf types
    vector<TLorentzVector> *Zdaughters;
    vector<TLorentzVector> *jet4v_antikt;
    vector<TLorentzVector> *hadron4v;
    vector<TLorentzVector> *parton4v;
    TLorentzVector  *z4v;
    vector<TLorentzVector> *photon4v;
    TLorentzVector  *jjg4v;
    vector<vector<TLorentzVector> > *jet_constituents;
    vector<string>  *jets_initParticule_mother_PDGIDs;
    Double_t        jet_radius;
    vector<double>  *weights;
    vector<int>     *Zdaughters_PID;
    
    // List of branches
    TBranch        *b_Zdaughters;   //!
    TBranch        *b_jet4v_antikt;   //!
    TBranch        *b_hadron4v;   //!
    TBranch        *b_parton4v;   //!
    TBranch        *b_z4v;   //!
    TBranch        *b_photon4v;   //!
    TBranch        *b_jjg4v;   //!
    TBranch        *b_jet_constituents;   //!
    TBranch        *b_jets_initParticule_mother_PDGIDs;   //!
    TBranch        *b_jet_radius;   //!
    TBranch        *b_weights;   //!
    TBranch        *b_Zdaughters_PID;   //!
    
    dump_into_ascii(TString filename, TString outname, TTree *tree=0);
    virtual ~dump_into_ascii();
    virtual Int_t    Cut(Long64_t entry);
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop();
    virtual Bool_t   Notify();
    virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef dump_into_ascii_cxx
dump_into_ascii::dump_into_ascii(TString filename, TString outname, TTree *tree) : fChain(0)
{
    // if parameter tree is not specified (or zero), connect the file
    // used to generate this class and read the Tree.
    outputASCIIFile = outname;
    inputASCIIFile = filename;
    if(inputASCIIFile.Contains("Signal")) is_signal = true;
    else if (inputASCIIFile.Contains("Background")) is_signal = false;
    else {
        cout << "Problem while determining whether file is signal or background !" << endl;
        return;
    }
    
    if (tree == 0) {
        file = (TFile*)gROOT->GetListOfFiles()->FindObject(filename);
        if (!file || !file->IsOpen()) {
            file = new TFile(filename);
        }
        file->GetObject("outtree",tree);
        
    }
    Init(tree);
}

dump_into_ascii::~dump_into_ascii()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t dump_into_ascii::GetEntry(Long64_t entry)
{
    // Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t dump_into_ascii::LoadTree(Long64_t entry)
{
    // Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
        Notify();
    }
    return centry;
}

void dump_into_ascii::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).
    
    // Set object pointer
    Zdaughters = 0;
    jet4v_antikt = 0;
    hadron4v = 0;
    parton4v = 0;
    z4v = 0;
    photon4v = 0;
    jjg4v = 0;
    jet_constituents = 0;
    jets_initParticule_mother_PDGIDs = 0;
    weights = 0;
    Zdaughters_PID = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
    
    fChain->SetBranchAddress("Zdaughters", &Zdaughters, &b_Zdaughters);
    fChain->SetBranchAddress("jet4v_antikt", &jet4v_antikt, &b_jet4v_antikt);
    fChain->SetBranchAddress("hadron4v", &hadron4v, &b_hadron4v);
    fChain->SetBranchAddress("parton4v", &parton4v, &b_parton4v);
    fChain->SetBranchAddress("z4v", &z4v, &b_z4v);
    fChain->SetBranchAddress("photon4v", &photon4v, &b_photon4v);
    fChain->SetBranchAddress("jjg4v", &jjg4v, &b_jjg4v);
    fChain->SetBranchAddress("jet_constituents", &jet_constituents, &b_jet_constituents);
    fChain->SetBranchAddress("jets_initParticule_mother_PDGIDs", &jets_initParticule_mother_PDGIDs, &b_jets_initParticule_mother_PDGIDs);
    fChain->SetBranchAddress("jet_radius", &jet_radius, &b_jet_radius);
    fChain->SetBranchAddress("weights", &weights, &b_weights);
    fChain->SetBranchAddress("Zdaughters_PID", &Zdaughters_PID, &b_Zdaughters_PID);
    Notify();
}

Bool_t dump_into_ascii::Notify()
{
    // The Notify() function is called when a new file is opened. This
    // can be either for a new TTree in a TChain or when when a new TTree
    // is started when using PROOF. It is normally not necessary to make changes
    // to the generated code, but the routine can be extended by the
    // user if needed. The return value is currently not used.
    
    return kTRUE;
}

void dump_into_ascii::Show(Long64_t entry)
{
    // Print contents of entry.
    // If entry is not specified, print current entry
    if (!fChain) return;
    fChain->Show(entry);
}
Int_t dump_into_ascii::Cut(Long64_t entry)
{
    // This function may be called from Loop.
    // returns  1 if entry is accepted.
    // returns -1 otherwise.
    return 1;
}
#endif // #ifdef dump_into_ascii_cxx
