#include <TLorentzVector.h>
#include <vector>

#ifdef __CLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ class vector<TLorentzVector>+;
#pragma link C++ class vector<vector<TLorentzVector>>+;
#endif
