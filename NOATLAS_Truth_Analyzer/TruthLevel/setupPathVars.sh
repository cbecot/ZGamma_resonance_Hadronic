CommonInstallDir     = /home/cbecot/Travail/NecessaryApps
HepMCdir             = $(CommonInstallDir)/HepMC_install
HepMClib             = -L$(HepMCdir)/lib -lHepMC
HepMCfiolib          = -L$(HepMCdir)/lib -lHepMCfio
FastJetLib           = -L/usr/local/lib -lfastjet
MCUtilsDir           = $(CommonInstallDir)/andybuckley-mcutils-1fb444ba7b70
HEPUtilsDir          = $(CommonInstallDir)/andybuckley-heputils-a56b04baa169
ROOTPath             = $(CommonInstallDir)/root/root_v6-04-16_src