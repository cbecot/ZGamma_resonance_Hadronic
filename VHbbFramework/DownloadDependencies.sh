USERNAME=cbecot
BASENAME=svn+ssh://$USERNAME@svn.cern.ch/reps
#RCRelease=2.4.18


svn co $BASENAME/atlasoff/PhysicsAnalysis/HiggsPhys/Run2/Hbb/CxAODFramework/FrameworkSub/trunk FrameworkSub

#svn co $BASENAME/atlasoff/PhysicsAnalysis/AnalysisCommon/PMGTools/tags/PMGTools-00-00-04 PMGTools


source FrameworkSub/bootstrap/setup-trunk.sh
svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/HiggsPhys/Run2/Hbb/InputsProcessingTools/PlottingTool/trunk PlottingTool
#rcSetup Base,$RCRelease
#rc find_packages
#rc compile
rc build


