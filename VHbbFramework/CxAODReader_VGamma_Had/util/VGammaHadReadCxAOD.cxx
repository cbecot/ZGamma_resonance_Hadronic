#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskList.h"
#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/ScanDir.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/ProofDriver.h"
#include "EventLoop/LSFDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/TorqueDriver.h"

#include "SampleHandler/Sample.h"

#include <stdlib.h> 
#include <vector> 
#include <iostream>

#include <TSystem.h> 
#include <TFile.h> 
#include <TChain.h> 

#include "CxAODTools/ConfigStore.h"
#include "CxAODReader_VGamma_Had/AnalysisReader_VGamma_Had.h"

#define DEBUG true

using namespace std;

void tag(SH::SampleHandler& sh, const std::string& tag);

int main(int argc, char* argv[]) {

  // Take the submit directory from the input if provided:
  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  if(argc < 2) {
    std::cout << " !!!--- MISSING ARGUMENTS ---!!! " << std::endl;
    std::cout << "--- arg #1) submit directory" << std::endl;
    std::cout << "--- arg #2) config path" << std::endl;
    std::cout << "--- arg #3) Samples" << std::endl;
    return 0;
  }
  
  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  std::string submitDir = "submitDir";
  std::string configPath = "data/CxAODReader_VGamma_Had/framework-read.cfg";
  std::string sample = "";
  if (argc > 1) submitDir = argv[1];
  if (argc > 2) configPath = argv[2];
  if (argc > 3) sample = argv[3];
  
  cout << "READER INFO : Will read samples " << sample << " with config " << configPath << " and write into " << submitDir << endl;

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // read run config
  static ConfigStore* config = ConfigStore::createStore(configPath);

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // Set up the job for xAOD access:
  xAOD::Init().ignore();
  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;


  std::string dataset_dir = config->get<std::string>("dataset_dir");
  
  cout << "Will lookup at following dataset_dir : " << dataset_dir << endl;
  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  bool eos;
  std::string prefix = "/eos/";
  if ( dataset_dir.substr(0, prefix.size()).compare(prefix) == 0 ) {
    eos = true;
    std::cout << "Will read datasets from EOS directory " << dataset_dir << std::endl;
  }	
  else {
    eos = false;
    std::cout << "Will read datasets from local directory " << dataset_dir << std::endl;
  }

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // Query - I had to put each background in a separate directory
  // for samplehandler to have sensible sample names etc.
  // Is it possible to define a search string for directories and assign all those to a sample name?
  // SH::scanDir (sampleHandler, list); // apparently it will come


  // create the sample handler
  SH::SampleHandler sampleHandler;
  std::vector<std::string>    sample_names;
  if (sample!=""){
    sample_names.push_back(sample);
  }
  else{
    config->getif<std::vector<std::string> >("sample_names", sample_names);
  }

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  if (sample.find("root://eosatlas") !=std::string::npos){
    TChain chain("CollectionTree");
    chain.Add(sample.c_str());
    sampleHandler.add(SH::makeFromTChain(submitDir.c_str(),chain));
  }
  else{
    if( sample_names.size()>0 ){
      for (unsigned int isamp(0) ; isamp<sample_names.size() ; isamp++) {
        std::string sample_name(sample_names.at(isamp));
        std::string sample_dir(dataset_dir+sample_name);
      
        // eos, local disk or afs
        if (!eos) {
          bool direxists=gSystem->OpenDirectory(sample_dir.c_str());
          if(!direxists){
            std::cout<<" ERROR no dir!!! "<< sample_dir << std::endl;
            continue;
          }
        }
        // eos, local disk or afs
        if(eos){
          std::cout<<" Reading from eos"<<std::endl;
          SH::DiskListEOS list(sample_dir,"root://eosatlas/"+sample_dir );
          SH::ScanDir()
            .samplePattern("*CxAOD*")
            .sampleName(sample_name)
            .scan(sampleHandler, list);
          std::cout<<" Reading from eos"<<std::endl;
        } else {
          SH::DiskListLocal list(sample_dir);
          SH::ScanDir()
            .samplePattern("*CxAOD*")
            .sampleName(sample_name)
            .scan(sampleHandler, list);
        }
        SH::Sample* sample_ptr = sampleHandler.get(sample_name);
        sample_ptr->meta()->setString("SampleID", sample_name);
        int nsampleFiles = sample_ptr->numFiles();
        
        std::cout<<" sample name "<< sample_name<<" with nfile "<<nsampleFiles<<std::endl;
      }
    }
    else{ // if no sample_name is specified, search the whole dataset_dir for any samples 

      // eos, local disk or afs
      if (!eos) {
        bool direxists=gSystem->OpenDirectory(dataset_dir.c_str());
        if(!direxists){
          std::cout<<" ERROR no dir!!! "<<dataset_dir<<" doesn't exist"<<std::endl;
          return 0; 
        }
      }

      // eos, local disk or afs
      if(eos){
        std::cout<<" Reading from eos"<<std::endl;
        SH::DiskListEOS list(dataset_dir,"root://eosatlas/"+dataset_dir );
        SH::ScanDir()
            .samplePattern("*CxAOD*")
            .scan(sampleHandler, list);
        std::cout<<" Reading from eos"<<std::endl;
      } else {
        SH::DiskListLocal list(dataset_dir);
        SH::ScanDir()
            .samplePattern("*CxAOD*")
            .scan(sampleHandler, list);
      }

    }
  }
  

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  cout << "PRINT THE SAMPLES THAT HAVE BEEN FOUND :" << endl;
  sampleHandler.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // remove submit dir before running the job
  //job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

  // create algorithm, set job options, maniplate members and add our analysis to the job:
  int analysisType = config->get<int>("analysisType");
  AnalysisReader* algorithm = nullptr;

  algorithm = new AnalysisReader_VGamma_Had();
  
  algorithm->setConfig(config);
  
  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  //limit number of events to maxEvents - set in config
  job.options()->setDouble (EL::Job::optMaxEvents, config->get<int>("maxEvents"));

  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop#Access_the_Data_Through_xAOD_EDM
  bool nominalOnly = false;
  config->getif<bool>("nominalOnly", nominalOnly);
  if (nominalOnly) {
    // branch access shows better performance for CxAODReader with Nominal only
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_branch);
  } else {
    // branch access cannot read shallow copies, so we need class access in case reading systematics
    job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);
  }

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // add algorithm to job
  job.algsAdd(algorithm);

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  // Run the job using the local/direct driver:
  EL::Driver* eldriver = 0;
  std::string driver = "direct";
  config->getif<std::string>("driver",driver);
  if (driver=="direct")
    eldriver = new EL::DirectDriver;
  else if (driver=="proof")
    eldriver = new EL::ProofDriver;
  else if (driver=="lsf"){
    eldriver = new EL::LSFDriver;
    //driver_batch->shellInit = "setupATLAS; rcSetup Base,2.3.14";
    job.options()->setString (EL::Job::optSubmitFlags, "-q 2nd -R \"swp > 15 && pool>10000\"");
  }
  else if (driver=="condor"){
    eldriver = new EL::CondorDriver;
    ((EL::CondorDriver*)eldriver)->shellInit = "export PATH=${PATH}:/usr/sbin:/sbin";
    job.options()->setDouble (EL::Job::optFilesPerWorker, 4);
    /*job.options()->setString (EL::Job::optSubmitFlags, "-dump dump" );
    job.options()->setString (EL::Job::optCondorConf,
                "requirements = Machine != \"xn05.hep.uiuc.edu\" \
            ");*/
  }
  else if (driver=="torque"){
    eldriver = new EL::TorqueDriver;
    //need medium6 for systematics
    job.options()->setString (EL::Job::optSubmitFlags, "-q medium6 ");
    //setting high to avoid multiple output files per sample
    //maybe ask event loop guys if there is a better way?
    //add file merge support and not just hist merge support to BatchDriver.cxx?
    double nFilesPerWorker = 100;
    config->getif<double>("nFilesPerWorker",nFilesPerWorker);
    eldriver->options()->setDouble("nc_EventLoop_FilesPerWorker", nFilesPerWorker);
  }
  else if (driver=="slurm"){
    system("mkdir -p ~/bin/; ln -s /usr/bin/sbatch ~/bin/bsub; export PATH=$PATH:~/bin");
    std::string slurmOptions = "-n 1 --cpus-per-task 1 --mem=2000 -p short -t 4:00:00";
    eldriver = new EL::LSFDriver;
    job.options()->setBool(EL::Job::optResetShell, false);
    job.options()->setString(EL::Job::optSubmitFlags, slurmOptions);
  }
  else {
    Error("VGammaReader", "Unknown driver '%s'", driver.c_str());
    return 0;
  }

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  eldriver->submit(job, submitDir);

  if(DEBUG) cout << __FILE__ << " " << __LINE__ << endl;
  return 0;
}
