#include <EventLoop/Worker.h>
#include <typeinfo>
#include "TSystem.h"
// new includes
#include <map>
#include <utility>

#include "FourMomUtils/xAODP4Helpers.h"

#include <CxAODReader_VGamma_Had/AnalysisReader_VGamma_Had.h>

#include "CxAODTools_VGamma_Had/VGamma_Had_MainEvtSelection.h"

#include "CxAODTools_VGamma_Had/DBProperties.h"
#include "CxAODTools/CommonProperties.h"

//#include "xAODMuon/MuonAuxContainer.h"

#define length(array) (sizeof(array)/sizeof(*(array)))

// compare_* functions for sorting jets
TLorentzVector m_lepVec;

bool compare_jet_byDeltaRlj(const xAOD::Jet* jet1,const xAOD::Jet* jet2){
  return jet1->p4().DeltaR(m_lepVec)<jet2->p4().DeltaR(m_lepVec);
}

bool compare_jet_pt(const xAOD::Jet* jet1,const xAOD::Jet* jet2){
  return jet1->pt()>jet2->pt();
}
bool compare_photon_pt(const xAOD::Photon* jet1,const xAOD::Photon* jet2){
  return jet1->pt()>jet2->pt();
}

EL::StatusCode AnalysisReader_VGamma_Had::setObjectsForOR (const xAOD::ElectronContainer *electrons,
                                                     const xAOD::PhotonContainer   *photons,
                                                     const xAOD::MuonContainer     *muons,
                                                     const xAOD::TauJetContainer   *taus,
                                                     const xAOD::JetContainer      *jets,
                                                     const xAOD::JetContainer      *fatJets) {
  if (m_debug) Info("setObjectsForOR()", "setObjectsForOR()");
  if (electrons) {
    for (const xAOD::Electron *elec : *electrons) {
      Props::passPreSel.set(elec, Props::isVHLooseElectron.get(elec));
    }
  }

  if (muons) {
    for (const xAOD::Muon *muon : *muons) {
      Props::passPreSel.set(muon, Props::isVHLooseMuon.get(muon));
    }
  }

  if (jets) {
    for (const xAOD::Jet *jet : *jets) {
      Props::passPreSel.set(jet, Props::isVetoJet.get(jet));
    }
  }

  if (fatJets) {
    for (const xAOD::Jet *jet : *fatJets) {
      Props::passPreSel.set(jet, Props::isFatJet.get(jet));
    }
  }

  if (taus) {
    for (const xAOD::TauJet *tau : *taus) {
      Props::passPreSel.set(tau, Props::passTauSelector.get(tau));
    }
  }

  if (photons) {
    for (const xAOD::Photon *photon : *photons) {
      Props::passPreSel.set(photon, Props::isVBFLoosePhoton.get(photon));
    }
  }

  return EL::StatusCode::SUCCESS;
} // setObjectsForOR
// 

/*** TODO : Modify this function ! ***/
EL::StatusCode AnalysisReader_VGamma_Had :: fill_VGamma_Had_Resolved()
{
  if (m_debug) Info("fill_VGamma_Had_Resolved()", "fill_VGamma_Had_Resolved()");
  /*** TODO : m_etree ***/
  bool is_to_be_filled=false; 


  //eventinfo variations? 
  const xAOD::EventInfo* eventInfo = m_eventInfoReader->getObjects("Nominal");

  // split the data and MC  between 2015 and 2016 
  string m_whichData = "2015"; 
  m_config->getif<string>("whichData", m_whichData);
  bool m_splitData = false; 
  m_config->getif<bool>("splitData", m_splitData);
  if( m_isMC && m_splitData ){
    if( m_whichData=="2015" && Props::PileupRdmRun.get(eventInfo)>296939 ){ 
      //std::cout<<"event skipped"<<std::endl; 
      return EL::StatusCode::SUCCESS; 
    }
    else if( m_whichData=="2016" && Props::PileupRdmRun.get(eventInfo)<296939 ) return EL::StatusCode::SUCCESS; 
  }

  int m_channelNumber=-1; 
  if( m_isMC ) m_channelNumber = eventInfo->mcChannelNumber(); 

  // TODO : move to config file
  bool debug = true;
  bool fillhisto = true;

  bool cutflow = (m_currentVar=="Nominal") ; // fill cutflow for only the nominal 


  /////////////////////////////////////////////////////////////
  //                   Retrieve Objects                      //
  /////////////////////////////////////////////////////////////

  DBResult selectionResult = ((VGamma_Had_MainEvtSelection*)m_eventSelection)->result();
  std::vector<const xAOD::Photon*> photon = selectionResult.ph;
  std::vector<const xAOD::Jet*> smallJets = selectionResult.smallJets;
  //std::vector<const xAOD::Jet*> trackJets = selectionResult.trackJets;

  /////////////////////////////////////////////////////////////
  //                   Event Preselection                    //
  /////////////////////////////////////////////////////////////

  /* initialize eventFlag - all cuts are initialized to false by default */
  unsigned long eventFlagPreselection = 0; // not used for the moment 
  unsigned long eventFlagResolved     = 0;


  if(cutflow) {
    /*** TODO : ??? ***/
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_combined", length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutAll", m_weight);
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_e",        length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutAll", m_weight);
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_m",        length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutAll", m_weight);
  }

  int m_RunNumber = -1;
  if(m_isMC) m_RunNumber = (int)(Props::PileupRdmRun.get(eventInfo));
  else m_RunNumber = eventInfo->runNumber(); 

  /* get the jets */ 
  std::vector<const xAOD::Jet*> sigJets;
  std::vector<const xAOD::Jet*> Jets;
  
  /* TLorentzVector for leading small jets */
  vector<TLorentzVector> smallJetVecs; 

  TLorentzVector jetVec;
  bool found_badjet=false;
  for(int iJet=0; iJet< smallJets.size(); iJet++){
    jetVec = smallJets[iJet]->p4();
    //if(jetVec.Pt()>20.e3 && jetVec.Pt()<50.e3 && !Props::goodJet.get(smallJets[iJet]) && (fabs( jetVec.Eta() )>2.4 || Props::Jvt.get(smallJets[iJet])>0.64) ) found_badjet=true;
    
    /*** TODO : Check the jet selection below ! ***/
    if(!Props::goodJet.get(smallJets[iJet])) {
      found_badjet=true;
      break;    
    } else if(jetVec.Pt()>20.e3 && jetVec.Pt()<60.e3 && (fabs( jetVec.Eta() )<2.4 || Props::Jvt.get(smallJets[iJet])<0.59) ) {
      found_badjet=true;
      break;
    }
    //if(jetVec.Pt()>50.e3 && DBProps::isBadJet.get(smallJets[iJet]) ) found_badjet=true;
    //if(jetVec.Pt()>60.e3) found_badjet=true;
    if(found_badjet) break;
    
    if( jetVec.Pt()>20.e3 && fabs(jetVec.Eta())<2.5 && Props::goodJet.get(smallJets[iJet]) && (fabs( jetVec.Eta() )>2.4 || Props::Jvt.get(smallJets[iJet])>0.59 || jetVec.Pt()>60.e3) ) sigJets.push_back(smallJets[iJet]);
    if( jetVec.Pt()>20.e3 && fabs(jetVec.Eta())<4.5 && Props::goodJet.get(smallJets[iJet]) && (fabs( jetVec.Eta() )>2.4 || Props::Jvt.get(smallJets[iJet])>0.59 || jetVec.Pt()>60.e3) ) Jets.push_back(smallJets[iJet]);
  }

  std::sort( Jets.begin(), Jets.end(), compare_jet_pt );
  int n_sigJetsAll = Jets.size();
  int n_sigJets = sigJets.size();
  std::sort( sigJets.begin(), sigJets.end(), compare_jet_pt );
  for(auto currentJet : Jets) smallJetVecs.push_back( currentJet->p4() );

  /* bad jet veto */ 
  if(found_badjet){
    if(m_debug) Info("fill_VGamma_Had_Resolved()","Failed bad jet veto");
    return EL::StatusCode::SUCCESS;
  }
  if(cutflow) {
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_combined", length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutBadJetVeto", m_weight);
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_e",        length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutBadJetVeto", m_weight);
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_m",        length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutBadJetVeto", m_weight);
  }



  int channel=-1;
  string channel_name="";

  /*** TODO : Trigger variations ! ***/

    // get variation 
    CP::SystematicVariation testSys(m_currentVar);
    CP::SystematicSet shiftSet(testSys.name());

  if(cutflow) {
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_combined", length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutLepPt", m_weight);
    m_histSvc->BookFillCutHistDual(Form("CutFlow_PreSelection_%s", channel_name.c_str()), length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutLepPt", m_weight);
  }


  /* get the triggers */ 
  string m_analysisMode = ""; 
  m_config->getif<string>("analysisMode", m_analysisMode);

  //  std::cout<<" running "<<m_analysisMode <<" Analysis"<<std::endl;

  //  int m_passHLT_e24_lhmedium_L1EM20VH = (m_isMC==0) ? Props::passHLT_e24_lhmedium_L1EM20VH.get(eventInfo) 
//   //                                                         : Props::passHLT_e24_lhmedium_L1EM18VH.get(eventInfo) ;
  
  /*** TODO : g120 trigger matching ! ***/
  int m_passHLT_g120_loose = Props::passHLT_g120_loose.get(eventInfo);

  int pass_Trigger_Resolved = 0;
  int pass_TriggerMatch_Resolved=1;
  if( m_RunNumber >= 296939 ){ // 2016 
    pass_Trigger_Resolved = m_passHLT_g120_loose;

  }
  else{ // 2015

    pass_Trigger_Resolved = m_passHLT_g120_loose;
  } 

  /* trigger cut */ 
  //if(!pass_Trigger || !pass_Trigger_Resolved){
  if( !pass_Trigger_Resolved ){
    if(m_debug) Info("fill_VGamma_Had_Resolved()","Failed g120 trigger");
    return EL::StatusCode::SUCCESS;
  }
  if(cutflow) {
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_combined", length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutTrigger", m_weight);
    m_histSvc->BookFillCutHistDual(Form("CutFlow_PreSelection_%s", channel_name.c_str()), length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutTrigger", m_weight);
  }

  /*** TODO : trigger weight ! ***/
  /* trigger match */ 
  //if(m_applyLepEffSF && m_isMC) m_weight*=trig_effSF;

  if(cutflow) {
    m_histSvc->BookFillCutHistDual("CutFlow_PreSelection_combined", length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutTriggerMatch", m_weight);
    m_histSvc->BookFillCutHistDual(Form("CutFlow_PreSelection_%s", channel_name.c_str()), length(VGamma_Had_PreSelectionCuts::Cuts_str), VGamma_Had_PreSelectionCuts::Cuts_str, "CutTriggerMatch", m_weight);
  }


  int pass_TwoSmallJets = (n_sigJetsAll>=2);
  if(!pass_TwoSmallJets){
  //  if(!pass_OneFatJet ){
    if(m_debug) Info("fill_VGamma_Had_Resolved()", "Failed 2 small jets cut");
    return EL::StatusCode::SUCCESS;
  }


  /////////////////////////////////////////////////////////////
  //                   Event Selection                       //
  /////////////////////////////////////////////////////////////


  //////////////////////////////////////////////
  /* common part for both merged and resolved */ 
  //////////////////////////////////////////////


  
  //////////////////////////////////////////////
  /*            Merged Analysis               */ 
  //////////////////////////////////////////////


  
  //////////////////////////////////////////////
  /*            Resolved Analysis             */ 
  //////////////////////////////////////////////


  //if( pass_TwoSmallJets && m_analysisMode=="Resolved" && !pass_OneFatJet){
  if( pass_TwoSmallJets && ( m_analysisMode=="Resolved" || ( m_analysisMode=="Combined" && pass_Trigger_Resolved && pass_TriggerMatch_Resolved) ) ){


    //**** Default Value - TODO: recover from config! ****//
    double JetsPtMin = 30.e3;
    double PhotonsPtMin = 100.e3;


    if(m_debug) Info("fill_VGamma_Had_Resolved()", "Resolved::Starting Analysis ...");

    for(int ijet=0; ijet<Jets.size(); ijet++){
      if(Jets[ijet]->p4().Pt()< JetsPtMin){
        Jets.erase( Jets.begin()+ijet );
        --ijet; 
      }
    }
    std::sort( Jets.begin(), Jets.end(), compare_jet_pt );
    for(int ijet=0; ijet<Jets.size(); ijet++) {
      m_histSvc->BookFillHist("jet_pt", 400, 0, 2000., Jets[ijet]->p4().Pt()/1000., m_weight);
      m_histSvc->BookFillHist("jet_eta", 100, -5, 5, Jets[ijet]->p4().Eta(), m_weight);

      std::string currJetStr( TString("jet"+TString::Itoa(ijet+1, 10)).Data() );
      m_histSvc->BookFillHist(currJetStr+"_pt", 2000, 0, 2000, Jets[ijet]->p4().Pt()/1000., m_weight);
      m_histSvc->BookFillHist(currJetStr+"_eta", 100, -5, 5, Jets[ijet]->p4().Eta(), m_weight);
    }

    for(int iph = 0 ; iph < photon.size() ; iph++) {
      if(photon[iph]->p4().Pt() < PhotonsPtMin) {
	photon.erase( photon.begin() + iph );
	--iph;
      }
    }
    std::sort(photon.begin(), photon.end(), compare_photon_pt);
    for(int iph=0 ; iph < photon.size(); iph++) {
      m_histSvc->BookFillHist("photon_pt", 400, 0, 2000., photon[iph]->p4().Pt()/1000., m_weight);
      m_histSvc->BookFillHist("photon_eta", 100, -5, 5, photon[iph]->p4().Eta(), m_weight);

      std::string currPhStr( TString("ph"+TString::Itoa(iph+1, 10)).Data() );
      m_histSvc->BookFillHist(currPhStr+"_pt", 2000, 0, 2000, photon[iph]->p4().Pt()/1000., m_weight);
      m_histSvc->BookFillHist(currPhStr+"_eta", 100, -5, 5, photon[iph]->p4().Eta(), m_weight);
    }
    
    double nJet30 = (Jets.size());
    m_histSvc->BookFillHist("nJet30", 100, 0, 100, nJet30, m_weight);
    //if(m_debug){     
    // TODO -***/
    //}      

    if(nJet30 > 1) {
      TLorentzVector dijet = Jets[0]->p4() + Jets[1]->p4();
      m_histSvc->BookFillHist("dijet_mass", 800, 0, 200, dijet.M()/1000., m_weight);
      m_histSvc->BookFillHist("dijet_pt", 1000, 0, 1000, dijet.Pt()/1000., m_weight);
      
      if(photon.size() > 0) {
	TLorentzVector threebody = dijet + photon[0]->p4();
	m_histSvc->BookFillHist("jjy_mass", 2000, 0, 2000, threebody.M()/1000., m_weight);
	m_histSvc->BookFillHist("jjy_pt", 2000, 0, 2000, threebody.Pt()/1000., m_weight);
      }
    }
    
    if (fabs(m_weight)>30)   return EL::StatusCode::SUCCESS;
    
  }// resolved analysis

  if(m_debug) Info("fill_VGamma_Had_Resolved()", "Done in fill_VGamma_Had_Resolved()");
  return EL::StatusCode::SUCCESS;
}

