#include <EventLoop/Worker.h>
#include <typeinfo>
#include "TSystem.h"

#include <CxAODReader_VGamma_Had/AnalysisReader_VGamma_Had.h>

#include "CxAODTools_VGamma_Had/VGamma_Had_MainEvtSelection.h"

#include "CxAODTools/CommonProperties.h"

#define length(array) (sizeof(array)/sizeof(*(array)))

ClassImp(AnalysisReader_VGamma_Had)

AnalysisReader_VGamma_Had::AnalysisReader_VGamma_Had() :
  m_hist_normalisation(nullptr)
{
}

EL::StatusCode AnalysisReader_VGamma_Had :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  if(m_debug) Info("histInitialize()", "Initializing DB histograms."); 
  m_config->getif<int>("analysisType", m_analysisType);
  if (m_debug)  Info("histInitialize()", "Analysis type %i", m_analysisType );

  // histogram manager
  m_histSvc = new HistSvc();
  m_histNameSvc = new HistNameSvc();
  m_histSvc -> SetNameSvc(m_histNameSvc);
  m_histSvc -> SetWeightSysts(&m_weightSysts);
  
  bool fillHists = true;
  m_config->getif<bool>("writeHistograms", fillHists);
  m_histSvc -> SetFillHists(fillHists);

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode AnalysisReader_VGamma_Had :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  //std::cout << "fileExecute Input file is " << wk()->inputFile()->GetName() << std::endl;
  //
  if(m_debug) Info("fileExecute()", "fileExecute()"); 

  TFile* inputfile=wk()->inputFile();
  TH1D* h = (TH1D*) inputfile->Get("MetaData_EventCount");
  if(h){
    if (!m_hist_normalisation) {
      m_hist_normalisation = (TH1D*) h->Clone();
      if (m_debug) Info("fileExecute()", "Made the cutflow hist"); 
    } else {
      m_hist_normalisation->Add(h);
    }
  }
  delete h;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VGamma_Had :: initializeSelection()
{
  if (m_debug) Info("initializeSelection()", "Initialize analysis '%i'.", m_analysisType);
  m_config->getif<int>("analysisType", m_analysisType);


  if (m_analysisType==VGamma_Had_Resolved) {
    m_eventSelection = new VGamma_Had_MainEvtSelection(m_config); 
    m_fillFunction = std::bind(&AnalysisReader_VGamma_Had::fill_VGamma_Had_Resolved, this);
    //AnalysisReader_VGamma_Had::histInitialize_1Lep();    
  } else {
    Error("initializeSelection()", "Invalid analysis type %i", m_analysisType);
    return EL::StatusCode::FAILURE;
  }
  
  
  /*** TODO : add a writeEasyTree bool in config ***/
  if(m_debug) Info("initializeSelection()", "Build an EasyTree");
  //m_etree = new EasyTree(false, m_analysisTypeName[m_analysisType], wk(), m_variations, true);
  //m_etree = new EasyTree(true, "test", wk(), m_variations, true);
  if(m_debug) Info("initializeSelection()", "EasyTree has been built and registered");
  
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_VGamma_Had :: initializeSumOfWeights()
{
  if (m_debug) Info("initializeSumOfWeights()", "Initialize sum of weights.");

  if (!m_isMC) {
    return EL::StatusCode::SUCCESS;
  }

  // which analysis
  std::string ana_read = "";
  if (m_analysisType == VGamma_Had_Resolved) ana_read = "VGamma_Had_Resolved";
  else {
    Warning("initializeSumOfWeights()", "Invalid analysis type %i", m_analysisType);
    return EL::StatusCode::FAILURE;
  }

  // COM energy
  std::string comEnergy = m_config->get<std::string>("COMEnergy");
//
  std::string sumOfWeightsFile = gSystem->Getenv("ROOTCOREBIN");
  sumOfWeightsFile += "/data/CxAODReader_VGamma_Had/yields.";
  sumOfWeightsFile += ana_read;
  sumOfWeightsFile += ".";
  sumOfWeightsFile += comEnergy;
  sumOfWeightsFile += ".DBL.txt";
  m_sumOfWeightsProvider = new sumOfWeightsProvider(sumOfWeightsFile);

  return EL::StatusCode::SUCCESS;
}

/*** TODO : readapt the next function ***/
EL::StatusCode AnalysisReader_VGamma_Had::initializeTools ()
{
  Info("initializeTools()", "Initialize tools.");
  // call the base 
  AnalysisReader::initializeTools(); 

  //m_PMGCorrsAndSysts = new PMGCorrsAndSysts(true);

  // trigger tool
  m_triggerTool = new TriggerTool(*m_config, "1lep" );
  EL_CHECK("AnalysisReader::initializeTools()", m_triggerTool->initialize());
  m_triggerSystList = m_triggerTool->getTriggerSystList();

  // temporary muon trigEffSF until TriggerTool is ready 
  m_muon_trig_sf_2015 = new CP::MuonTriggerScaleFactors("myTrigSFClass2015");
  m_muon_trig_sf_2015->msg().setLevel( MSG::ERROR );
  if(m_debug) m_muon_trig_sf_2015->msg().setLevel( MSG::DEBUG );
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2015->setProperty("MuonQuality", "Medium"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2015->setProperty("Year", "2015"));
  //TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2015->setProperty("mc", "mc15c"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2015->setProperty("Isolation", "IsoTight"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2015->initialize());

  m_muon_trig_sf_2016 = new CP::MuonTriggerScaleFactors("myTrigSFClass2016");
  m_muon_trig_sf_2016->msg().setLevel( MSG::ERROR );
  if(m_debug) m_muon_trig_sf_2016->msg().setLevel( MSG::DEBUG );
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2016->setProperty("MuonQuality", "Medium"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2016->setProperty("Year", "2016"));
  //TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2016->setProperty("mc", "mc15c"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2016->setProperty("Isolation", "IsoTight"));
  TOOL_CHECK("AnalysisReader_DB::initializeSelection()",m_muon_trig_sf_2016->initialize());
 
  return EL::StatusCode::SUCCESS;
} //initializeTools


EL::StatusCode AnalysisReader_VGamma_Had :: finalize ()
{
  if (m_debug) Info("finalize()", "finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  //if(m_pileupreweighting){
  //  delete m_pileupreweighting;
  //  m_pileupreweighting=nullptr;
  //}
 
  //if(m_muon_trig_sf){
  //  delete m_muon_trig_sf;
  //  m_muon_trig_sf=nullptr;
  //}

  // call base to print number of events
  AnalysisReader::finalize();

  //if(m_PMGCorrsAndSysts){
  //  delete m_PMGCorrsAndSysts;  
  //  m_PMGCorrsAndSysts=nullptr;  
  //}

  if(m_muon_trig_sf_2015){
    delete m_muon_trig_sf_2015;
    m_muon_trig_sf_2015=nullptr;
  }
  if(m_muon_trig_sf_2016){
    delete m_muon_trig_sf_2016;
    m_muon_trig_sf_2016=nullptr;
  }

 
  wk()->addOutput(m_hist_normalisation);


  return EL::StatusCode::SUCCESS;
}

// function to create the bit mask
unsigned int AnalysisReader_VGamma_Had::bitmask(const unsigned int cut, const std::vector<unsigned int> &excludeCuts) {
  if (m_debug) Info("bitmask()", "bitmask()");
  
  unsigned int mask = 0;
  unsigned int bit  = 0;
  
  for (unsigned int i=0; i < cut+1; ++i) {
    if   ( std::find(excludeCuts.begin(), excludeCuts.end(), i) != excludeCuts.end() ) {
      // if a cut should be excluded set the corresponding bit to 0
      mask = mask | 0 << bit++;
    } else  { 
      // otherwise set the bit to 1
      mask = mask | 1 << bit++;
    }  
  }
  
  return mask;
}

// function to compare a bit flag against a bit mask
// Usage: given a flag, check if it passes all cuts up to and including "cut"
// excluding the cuts in "excludedCuts"
bool AnalysisReader_VGamma_Had::passAllCutsUpTo(const unsigned long int flag, const unsigned int cut, const std::vector<unsigned int> &excludeCuts) {
  if (m_debug) Info("passAllCutsUpTo()", "passAllCutsUpTo()");
  
  // Get the bitmask: we want to check all cuts up to "cut" excluding the cuts listed in excludeCuts
  unsigned int mask = bitmask(cut, excludeCuts);
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;
  
}

// a function to check specifig cuts
bool AnalysisReader_VGamma_Had::passSpecificCuts(const unsigned long int flag, const std::vector<unsigned int> &cuts) { 
  if (m_debug) Info("passSpecificCuts()", "passSpecificCuts()");
  unsigned int mask = 0;
  // Make the bit mask
  for (auto cut : cuts) mask = mask | 1 << cut;
  // Check if the flag matches the bit mask
  return (flag & mask) == mask;
}


// a function to update the bit flag
void AnalysisReader_VGamma_Had::updateFlag(unsigned long int &flag, const unsigned int cutPosition, const bool passCut) { 
  if (m_debug) Info("updateFlag()", "updateFlag()");
  // Put bit passCut (true or false) at position cutPosition
  flag = flag | passCut << cutPosition;
}

