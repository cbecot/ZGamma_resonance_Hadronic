#ifndef CxAODReader_AnalysisReader_VGamma_Had_H
#define CxAODReader_AnalysisReader_VGamma_Had_H

#include "CxAODReader/AnalysisReader.h"
#include "CxAODReader/EasyTree.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
//#include "PMGTools/PMGCorrsAndSysts.h"

#include <TH1F.h>


class AnalysisReader_VGamma_Had : public AnalysisReader {
public:
  std::vector<std::string> m_analysisTypeName = {"VGamma_Had_Resolved"};
  enum Type {VGamma_Had_Resolved};

protected:

  virtual EL::StatusCode initializeSelection() override;
  virtual EL::StatusCode initializeSumOfWeights() override;
  virtual EL::StatusCode initializeTools() override;

  virtual EL::StatusCode setObjectsForOR(
          const xAOD::ElectronContainer*,
          const xAOD::PhotonContainer*,
          const xAOD::MuonContainer*,
          const xAOD::TauJetContainer*,
          const xAOD::JetContainer*,
          const xAOD::JetContainer*) override;
//  { return EL::StatusCode::SUCCESS; }

  EL::StatusCode fill_VGamma_Had_Resolved();
  
  EL::StatusCode fill_VGamma_Had_Resolved_cutflow(std::string label, int ch=-1);

  EL::StatusCode histInitialize_VGamma_Had_Resolved();

  EL::StatusCode clearVectors();

  int m_analysisType; //!

  //PMGCorrsAndSysts* m_PMGCorrsAndSysts;
  EasyTree       *m_etree    = nullptr; // !


  //CP::PileupReweightingTool *m_pileupreweighting; //!
  CP::MuonTriggerScaleFactors *m_muon_trig_sf_2015; //!
  CP::MuonTriggerScaleFactors *m_muon_trig_sf_2016; //!

  TH1D* m_hist_normalisation; //!

  //1Lep histo
  TH1D* m_hist_1lep_CutFlow[3]; //!
  TH1D* m_hist_1lep_CutFlow_noWght[3]; //!  unweighted version

  typedef std::tuple<uint32_t, unsigned long long, float, string> RunEventMet; //!
  std::set<RunEventMet> m_setRunEventMet; //!

  // trigger sf systematics
  std::vector<std::string> m_triggerSystList; 

  // a function to check whether an event passes
  // all the cuts up to "cut" - possibility to exclude some cuts
  bool passAllCutsUpTo(const unsigned long int flag, const unsigned int cut, const std::vector<unsigned int> &excludeCuts={});
  
  // a function to check whether an event passes a specific set of cuts
  bool passSpecificCuts(const unsigned long int flag, const std::vector<unsigned int> &cuts);
  
  // a function to create the bit mask
  unsigned int bitmask(const unsigned int cut, const std::vector<unsigned int> &excludeCuts={});
  
  // a function to update the bit flag
  void updateFlag(unsigned long int &flag, const unsigned int cutPosition, const bool passCut=1);

public:
  AnalysisReader_VGamma_Had();
  virtual EL::StatusCode histInitialize () override;
  virtual EL::StatusCode fileExecute () override;
  virtual EL::StatusCode finalize () override;

  // this is needed to distribute the algorithm to the workers
  ClassDef(AnalysisReader_VGamma_Had, 1);
};



/**********************************************/
/*** TODO : Readapt or remove what is below ***/
/**********************************************/
namespace VGamma_Had_PreSelectionCuts {

  enum Cuts {
    All = 0,                    // P0
    BadJetVeto,                 // P1
    SecondLepVeto,              // P2
    LepPt,                      // P3
    isWHSignalLep,              // P4
    Trigger,                    // P5
    TriggerMatch,               // P6
  };

  static std::string Cuts_str[] {
    "CutAll",                   // P0
    "CutBadJetVeto",            // P1
    "CutSecondLepVeto",         // P2
    "CutLepPt",                 // P3
    "CutLepID",                 // P4
    "CutTrigger",               // P5
    "CutTriggerMatch",          // P6
  };

}

namespace VGamma_Had_MergedCuts {

  enum Cuts {
    AtLeastOneFatJet = 0,       // M0
    MET_100GeV,                 // M1
    pTW_200GeV,                 // M2
    pTW_MlvJ,                   // M3
    pTJ_MlvJ,                   // M4
    isWZJet,                    // M5
    SRInclusiveHP,              // M6, high purity 
    SRWZHP,                     // M7 
    SRWWHP,                     // M8
    TopCtrlHP,                  // M9
    WCtrlHP,                    // M10 
    SRInclusiveLP,              // M11, low purity 
    SRWZLP,                     // M12
    SRWWLP,                     // M13
    TopCtrlLP,                  // M14
    WCtrlLP,                    // M15 
  };

  static std::string Cuts_str[] {
    "CutAtLeastOneFatJet",      // M0
    "CutMET",                   // M1
    "CutpTW",                   // M2
    "CutpTWMlvJ",               // M3
    "CutpTJMlvJ",               // M4
    "CutisWZJet",               // M5
    "SRInclusiveHP",            // M6, high purity 
    "SRWZHP",                   // M7 
    "SRWWHP",                   // M8
    "TopCtrlHP",                // M9
    "WCtrlHP",                  // M10 
    "SRInclusiveLP",            // M11, low purity 
    "SRWZLP",                   // M12
    "SRWWLP",                   // M13
    "TopCtrlLP",                // M14
    "WCtrlLP",                  // M15
  };

}





// place holders, to be filled up 
namespace Vgamma_Had_ResolvedCuts {

  enum Cuts {
    TwoSmallJets = 0,         // R0
    MET_100GeV,                // R1
    TwoJets30,                // R2
    Found_Whad,               // R3
    Found_Whad_15GeV,         // R4
    bJetsVeto,                // R5
    DeltaPhi_Wj1j2,           // R6
    DeltaPhi_WljX,            // R7
    DeltaPhi_WlMET,           // R8
    DeltaPhi_WjXMET,          // R9
    Wmasswindow,              // R10
    DecayTopo,                // R11
    FourJets30,               // R12
    Found_VBF,                // R13
    VBFMjjCut,                // R14
    VBFDYjjCut,               // R15
    VBFSR,                    // R16
    VBFSR_All,                // R17
    VBF_WCR_VBF_PreSel,       // R18
    VBF_WCR_MET,              // R19
    VBF_WCR_VBF,               // R20
    VBF_WCR_MW,                // R21
    VBF_WCR,                   // R22
    VBF_WCR_DecTop,            // R23
    VBF_TopCR_nJets,           // R24
    VBF_TopCR_Btag,            // R25
    VBF_TopCR_MET,             // R26
    VBF_TopCR_VBFMjj,          // R27
    VBF_TopCR_VBFDYjj,         // R28
    VBF_TopCR_MW,              // R29
    VBF_TopCR,                 // R30
    VBF_TopCR_DecTop,          // R31
    //    GGFSR,                     // R32
    //    GGF_WCR,                   // R33 
    //    GGF_TopCR,                 // R34
  };

  static std::string Cuts_str[] {
    "Resolved_CutTwoSmallJets",          // R0
    "Resolved_CutMET",                   // R1
    "Resolved_CutTwoJets30",             // R2
    "Resolved_Found_Whad",               // R3
    "Resolved_Found_Whad_15GeV",         // R4
    "Resolved_CutbJetsVeto",             // R5
    "Resolved_CutDeltaPhi_Wj1j2",        // R6
    "Resolved_CutDeltaPhi_WljX",         // R7
    "Resolved_CutDeltaPhi_WlMET",        // R8
    "Resolved_CutDeltaPhi_WjXMET",       // R9
    "Resolved_CutWmasswindow",           // R10
    "Resolved_CutDecayTopo",             // R11
    "Resolved_CutFourJets30",            // R12
    "Resolved_Found_VBF",                // R13
    "Resolved_CutVBFMjj",                // R14
    "Resolved_CutVBFDYjj",               // R15
    "Resolved_VBFSR",                    // R16
    "Resolved_VBFSR_All",                // R17
    "Resolved_VBF_WCR_VBF_PreSel",       // R18
    "Resolved_VBF_WCR_MET",              // R19
    "Resolved_VBF_WCR_VBF",              // R20
    "Resolved_VBF_WCR_MW",               // R21
    "Resolved_VBF_WCR",                  // R22
    "Resolved_VBF_WCR_DecTop",           // R23
    "Resolved_VBF_TopCR_nJets",          // R24
    "Resolved_VBF_TopCR_Btag",           // R25
    "Resolved_VBF_TopCR_MET",            // R26
    "Resolved_VBF_TopCR_VBFMjj",         // R27
    "Resolved_VBF_TopCR_VBFDYjj",        // R28
    "Resolved_VBF_TopCR_MW",             // R29
    "Resolved_VBF_TopCR",                // R30
    "Resolved_VBF_TopCR_DecTop",         // R31
      //    "Resolved_GGFSR",                    // R32
      //    "Resolved_GGF_WCR",                  // R33
      //    "Resolved_GGF_TopCR",                // R34
 };

}


// place holders, to be filled up 

#endif
