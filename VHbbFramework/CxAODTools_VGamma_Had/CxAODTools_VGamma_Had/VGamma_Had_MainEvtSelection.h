#ifndef CxAODTools_VGamma_Had_MainEvtSelection_H
#define CxAODTools_VGamma_Had_MainEvtSelection_H

#include "CxAODTools_VGamma_Had/DBEvtSelection.h"
#include "CxAODTools/ConfigStore.h"


class VGamma_Had_MainEvtSelection : public DBEvtSelection
{

public:
  VGamma_Had_MainEvtSelection(ConfigStore* config);
  virtual ~VGamma_Had_MainEvtSelection() noexcept {}
  
  virtual bool passSelection(SelectionContainers& containers, bool isKinVar);
  
  virtual bool passPreSelection(SelectionContainers& containers, bool isKinVar);

  virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                                   xAOD::EventInfo* eventInfoOut,
                                   bool isKinVar,
                                   bool isWeightVar,
                                   std::string sysName,
                                   int rdm_RunNumber,
                                   CP::MuonTriggerScaleFactors* trig_sfmuon) override;
  
 private:
  //Config store
  ConfigStore* m_config;

  bool m_debug;

  //leading and sub-leading leptons
  const xAOD::Electron* el_1;
  const xAOD::Muon* mu_1;
  const xAOD::Electron* el_2;
  const xAOD::Muon* mu_2;

  std::vector<const xAOD::Photon*> _ph;
  std::vector<const xAOD::Muon*> _mu;

  //Cut variables(configurable via config file)
  double m_ptAsymCut;
  double m_deltaYCut;
  double m_YFiltCut;
  std::string m_softTerm;
  std::string m_analysisMode; 
  std::vector<std::string> m_TriggerList;

  void SetParameters();
  
  int passTriggerSelection(const xAOD::EventInfo* evtinfo);
  int passJetSelection(); //const xAOD::JetContainer* fatjets);
  int passJetPreSelection(const xAOD::JetContainer* akt4jets, const xAOD::JetContainer* fatjets, const xAOD::JetContainer* trackjets);
};

#endif
