#ifndef CxAODTools_VGamma_Had_MainEvtPreSelection_H
#define CxAODTools_VGamma_Had_MainEvtPreSelection_H

#include "CxAODTools_VGamma_Had/DBEvtSelection.h"
#include "CxAODTools/ConfigStore.h"


class MainEvtPreSelection : public DBEvtSelection
{

public:
  MainEvtPreSelection(ConfigStore* config);
  virtual ~MainEvtPreSelection() noexcept {}
  
  virtual bool passSelection(SelectionContainers& containers, bool isKinVar);
  
  virtual bool passPreSelection(SelectionContainers& containers, bool isKinVar);

  virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                                   xAOD::EventInfo* eventInfoOut,
                                   bool isKinVar,
                                   bool isWeightVar,
                                   std::string sysName,
                                   int rdm_RunNumber,
                                   CP::MuonTriggerScaleFactors* trig_sfmuon) override;
  
 private:
  //Config store
  ConfigStore* m_config;

  //leading and sub-leading leptons

  //std::vector<const xAOD::Electron*> _el;
  //std::vector<const xAOD::Muon*> _mu;

  //Cut variables(configurable via config file)
  std::string m_softTerm;
  std::vector<std::string> m_TriggerList;

  bool m_applyJetPreSelection;
  bool m_applyLepPreSelection;
  bool m_applyMETCut;
  int  m_numberOfLeptons;

  bool m_debug;

  std::string m_electron_id_level;
  std::string m_muon_id_level;

  float m_electron_pt;
  float m_muon_pt;
  float m_electron_eta;
  float m_muon_eta;

  void SetParameters();
  
  int passTriggerSelection(const xAOD::EventInfo* evtinfo);
  int passJetSelection(const xAOD::JetContainer* fatjets);
  int passJetPreSelection(const xAOD::JetContainer* akt4jets, const xAOD::JetContainer* fatjets, const xAOD::JetContainer* trackjets);
};

#endif
