#include "CxAODTools_VGamma_Had/DBEvtSelection.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
//todo: add tau, photon?
//#include "xAODEventInfo/EventInfo.h"
#include "CxAODTools/CommonProperties.h"

#include "TVector2.h"
#include <iostream>

DBEvtSelection::DBEvtSelection()//ConfigStore* config)
  : EventSelection()
{



}

void DBEvtSelection::clearResult() {
//change so we are clearing the items in the DBResult struct
  m_result.pass = false;
  m_result.signalJets.clear();
  m_result.forwardJets.clear();
  m_result.smallJets.clear();
  m_result.largeJets.clear();
  m_result.trackJets.clear();
  m_result.ph.clear();
  m_result.el.clear();
  m_result.mu.clear();
  m_result.met = nullptr;
}

int DBEvtSelection::doVGammaPhotonPreSelection(const xAOD::PhotonContainer* photons,
                               std::vector<const xAOD::Photon*>& ph){

  ph.clear();
  int nPhotonsSig=0;

  for (unsigned int iPhotons(0) ; iPhotons < photons->size(); ++iPhotons) {
    const xAOD::Photon * current_ph = photons->at(iPhotons);
    if ( !Props::passOR.get(current_ph) ) continue;
    //if( Props::OROutputLabel.get(elec) ) continue;
    //if( fabs(DBProps::ElClusterEta.get(elec)) < 1.52 && fabs(DBProps::ElClusterEta.get(elec))>1.37 ) continue;
    //if( elec->caloCluster() && fabs( elec->caloCluster()->etaBE(2) ) < 1.52 && fabs( elec->caloCluster()->etaBE(2) )>1.37 ) continue;
    //if( !elec->caloCluster() && fabs( elec->eta() ) < 1.52 && fabs( elec->eta() )>1.37 ) continue;
    //if (DBProps::isVVLooseElectron.get(elec)) {
    if ( !Props::isVBFLoosePhoton.get(current_ph) ) continue;
    if ( !Props::isTight.get(current_ph) ) continue;
    if ( current_ph->pt() < 140*1000. ) continue;
    if( !Props::isFixedCutTightCaloOnlyIso.get(current_ph) ) continue;
    
    nPhotonsSig++;
    ph.push_back(current_ph);
    
  }



  if (nPhotonsSig == 0){
    return 0;
  } else if (nPhotonsSig==1) {
    return 1;
  } else if (nPhotonsSig>=2) {
    return 2;
  }
  
  return -1;
}

bool DBEvtSelection::passKinematics() {
  // MJ cuts, like MET / MPT etc...
  // my advice is to add in passKinematics() prototype all the stuff that
  // doesn't need to be put in the Result struct, like MPT

  return true;
}

bool DBEvtSelection::passJetCleaning(const xAOD::JetContainer* jets)
{
  bool result = true;
  for(const auto j : *jets) {
    if (!Props::goodJet.get(j)) {
      result = false;
      break;
    }
  }
  return result;
}


bool DBEvtSelection::doJetCleaning(const xAOD::JetContainer* jets) {
  bool found_badjet = false;
  for (const xAOD::Jet * jet : *jets) {
    //if (!DBProps::isBadJet.get(jet)) continue;
    if ( Props::goodJet.get(jet)) continue;
    if(jet->pt() > 50e3) {
        found_badjet = true;
    } else if (jet->pt() > 20e3 && (fabs(jet->eta()) > 2.4 || Props::Jvt.get(jet)>0.64)) {
        found_badjet = true;
    }
  }
  return !found_badjet;
}


