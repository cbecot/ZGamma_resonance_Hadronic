// VVlvJ includes
#include "CxAODTools_VGamma_Had/VGamma_Had_MainEvtSelection.h"
#include "CxAODTools_VGamma_Had/DBProperties.h"
#include "CxAODTools/CommonProperties.h"

VGamma_Had_MainEvtSelection::VGamma_Had_MainEvtSelection(ConfigStore* config)
  : m_ptAsymCut(0.15), 
    m_deltaYCut(1.2), 
    m_YFiltCut(0.45),
    m_config(config), 
    DBEvtSelection()
{
  SetParameters();
}

void VGamma_Had_MainEvtSelection::SetParameters()
{
  m_config->getif<bool>("debug", m_debug);
  m_config->getif<double>("VVJJptAsymCut", m_ptAsymCut);
  m_config->getif<double>("VVJJdeltaYCut", m_deltaYCut);
  m_config->getif<double>("VVJJYFiltCut", m_YFiltCut);
  m_config->getif<std::vector<std::string> >("triggerListDB", m_TriggerList);
  m_config->getif<std::string>("METSoftTerm", m_softTerm);
  m_config->getif<std::string>("analysisMode", m_analysisMode);
}

bool VGamma_Had_MainEvtSelection::passSelection(SelectionContainers& containers, bool isKinVar) {
  // we don't use this method, but have to provide an implementation 
  
  
  //const xAOD::EventInfo* evtinfo = containers.evtinfo;
  //const xAOD::MissingET* met = containers.met;
  //const xAOD::ElectronContainer* electrons = containers.electrons;
  //  const xAOD::PhotonContainer* photons = containers.photons;
  //const xAOD::MuonContainer* muons = containers.muons;
  //  const xAOD::TauJetContainer* taus = containers.taus;
  //const xAOD::JetContainer* jets = containers.jets;
  //const xAOD::JetContainer* fatjets = containers.fatjets;
  //const xAOD::JetContainer* trackjets = containers.trackjets;

  //clearResult();

  //m_result.met=met;
  //if (met->met() < 20000){
  //    return false;
  //}

  //bool pass = passJetPreSelection(jets, fatjets, trackjets) && (doDBLeptonSelection(_el, _mu) == 1);
  //return pass;
  return true;

}


bool VGamma_Had_MainEvtSelection::passPreSelection(SelectionContainers& containers, bool isKinVar) {

  //return true;
  const xAOD::EventInfo* evtinfo = containers.evtinfo;
  const xAOD::MissingET* met = containers.met;
  const xAOD::ElectronContainer* electrons = containers.electrons;
  const xAOD::PhotonContainer* photons = containers.photons;
  const xAOD::MuonContainer* muons = containers.muons;
  const xAOD::TauJetContainer* taus = containers.taus;
  const xAOD::JetContainer* jets = containers.jets;
  const xAOD::JetContainer* fatjets = containers.fatjets;
  const xAOD::JetContainer* trackjets = containers.trackjets;

  clearResult();
  
  if(m_debug) Info("passPreSelection()", "In Pre Selection");
  m_cutFlow.count("Full");

  m_result.met = met;

  // This method is called from the HSG5framework and determines whether this event should be kept or skipped.
  // Note: If any sys. variation flags the event to be kept, it will be kept. So the event selection (and OR)
  // needs to be performed again in subsequent analysis (if writting the CxAOD to disk)

  // return values:
  // 0 : didn't pass selection
  // 1 : passed selection

  // event cleaning
  bool passEventCleaning = true;
  bool isMC = Props::isMC.get(evtinfo);
  passEventCleaning &= Props::hasPV.get(evtinfo);
  if ( ! isMC ) {
    passEventCleaning &= Props::passGRL.get(evtinfo);
    passEventCleaning &= Props::isCleanEvent.get(evtinfo);
  }


  if( ! passEventCleaning ) return false;
  if(m_debug) Info("passPreSelection()", "Finished Event Cleaning");
  m_cutFlow.count("EventCleaning");

  m_result.ph.clear();
  _ph.clear();

  if(m_debug) Info("passPreSelection()", "Will now run doVGammaPhotonPreSelection");
  int ph_result = doVGammaPhotonPreSelection(photons, _ph); 

  ph_result = _ph.size();

  
  if(ph_result<=0) {
    if(m_debug) Info("passPreSelection()", "Failed Photon PreSelection");
    return false;
  } else if ((ph_result >= 1) && m_debug) {
    Info("passPreSelection()", "Passed Photon PreSelection");
    //return false;
  }
  m_cutFlow.count("AtLeastOnePhoton");


  m_result.ph = _ph;

  // jets
//  if( !passJetPreSelection( jets, fatjets, trackjets ) ){
  //  // very loose jet cut for first iteration
  //  // at least one fat jet or at least two akt4 jets
//    Info("passPreSelection()", "Failed Jet Preselection");
//    return false;
//  }
  if(m_debug) Info("passPreSelection()", "Will now run jet preselection");
  if( !passJetPreSelection( jets, fatjets, trackjets ) ){
    // currently don't do anything on jets but filling the jet containers in m_result
  }
  if(m_debug) Info("passPreSelection()", "Passed Jet PreSelection");
  m_cutFlow.count("JetPreRequirement");

  // trigger
  //if ( !passTriggerSelection(const xAOD::EventInfo* evtinfo) ) return false;
  if(m_debug) Info("passPreSelection()", "Passed Trigger Selection");
  m_cutFlow.count("Trigger");

  return true;
}
  
EL::StatusCode VGamma_Had_MainEvtSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                                    xAOD::EventInfo* eventInfoOut,
                                    bool isKinVar,
                                    bool isWeightVar,
                                    std::string sysName,
                                    int rdm_RunNumber,
                                    CP::MuonTriggerScaleFactors* trig_sfmuon)
{
  return EL::StatusCode::SUCCESS;
}

// ----------- Selection Functions -------------//

//  if(m_TriggerList.size()){
//    if( !(DBProps::passHLT_mu26_imedium.get(evtinfo) ||
//	  DBProps::passHLT_mu50.get(evtinfo) ||
//	  DBProps::passHLT_e28_tight_iloose.get(evtinfo) ||
//	  DBProps::passHLT_e60_medium.get(evtinfo) ) ){
//      return 0;
//    }
//  }
//  return 1;
//}

int VGamma_Had_MainEvtSelection::passJetPreSelection(const xAOD::JetContainer* akt4jets, const xAOD::JetContainer* fatjets, const xAOD::JetContainer* trackjets){
  m_result.smallJets.clear();
  m_result.largeJets.clear();
  m_result.trackJets.clear();


  int njets=0, nfatjets=0;
  for( unsigned int iJet=0; iJet < akt4jets->size(); ++iJet ){
    const xAOD::Jet* jet = akt4jets->at(iJet);
    if( !Props::goodJet.exists(jet) ) continue;
    //if( Props::OROutputLabel.get(jet) ) continue;
    if( !Props::passOR.get(jet) ) continue;
    m_result.smallJets.push_back(jet);
    njets++;
  }
  std::sort(m_result.smallJets.begin(), m_result.smallJets.end(), sort_pt);

  if(m_analysisMode!="Resolved") {  
    for(unsigned int iFatjet=0; iFatjet < fatjets->size(); ++iFatjet ){
      const xAOD::Jet* fatjet = fatjets->at(iFatjet);
      //if( !Props::passPreSel.get(fatjet)) continue; //Props::passPreSel applies pt/eta cut
      if( !Props::isFatJet.get(fatjet)) continue; //Props::isFatJet applies pt/eta cut
      m_result.largeJets.push_back(fatjet);
      nfatjets++;
    }
    std::sort(m_result.largeJets.begin(), m_result.largeJets.end(), sort_pt);
  }

  if(trackjets){
    for (unsigned int iJet(0) ; iJet < trackjets->size(); iJet++) {
        //apply some selection?
        const xAOD::Jet * jet = trackjets->at(iJet);
            m_result.trackJets.push_back(jet);
    }
  }

  if(njets < 2) return 0;
  //if( nfatjets < 1 && njets < 2 ) return 0;
  //if( nfatjets < 1 || njets < 1 ) return 0;
  //if( nfatjets<1 ) return 0;

  return 1;
}

/*** TODO : Reimplement next function and find where it is called ***/
int VGamma_Had_MainEvtSelection::passJetSelection() //*const xAOD::JetContainer* fatjets*/)
{
  /*if(fatjets->size() < 1) { return 0; }
  m_cutFlow.count("JetContainer Size");

  //Assumes jets are sorted by pt - need to check
  const xAOD::Jet* fatjet = fatjets->at(0);
  
  //Check both jets pass pt, eta cuts
  if(!Props::passPreSel.get(fatjet)) { return 0; }
  m_cutFlow.count("FatJet pt/eta");

  //Check jets match a dijet topology (probably want to split these up somehow) 
  const bool l_isW = Props::isWJet.get(fatjet);
  const bool l_isZ = Props::isZJet.get(fatjet);

  if (l_isW || l_isZ ){
    m_cutFlow.count("Diboson");
    return 1;
  }*/

  return 1;
}
